'use strict';


var Hapi = require('hapi');
const mongojs = require('mongojs');
const hostIP = '172.26.8.11'; //Your Server's IP
const mongouser = 'secure'; //Your Mongo username
const mongopassword = 'S3cur3Us3B1tch!'; //Your Mongo Password
const dbName = 'amdin'; //Your Database Name

var Path = require('path');
var connectionString = mongouser.concat(':').concat(mongopassword).concat('@127.0.0.1:27017/').concat(dbName).concat('?authSource=admin');
// Create a server with a host and port
var server = new Hapi.Server();
server.connection({
	host: hostIP,
    port: 3000
});

var io = require('socket.io')(server.listener);
//Connect to a db
// server.app.db = mongojs('zoechatplus', ['User']);  //<--- Added
server.app.db = mongojs(connectionString, ['User']);

/*

  require('./routes/chat'),
  require('inert'),
  require('hapi-error')
*/
//Load plugins and start server
server.register([require('inert'),require('./routes/user'),require('./routes/api'),require('./routes/web'),require('./routes/admin'),require('hapi-error'), require('vision')], (err) => {

  if (err) {
    throw err;
  }

 server.views({
    engines: {
      html: require('handlebars') // or Jade or Riot or React etc. 
    },
    path: Path.resolve(__dirname, './')
  });

server.route([
    { method: 'GET', path: '/', vhost: 'web.zoechat.com', handler: { file: "./zoechatweb/index.html" } },
    // switch these two routes for a /static handler?
    { method: 'GET', path: '/css/animate.css', vhost: 'web.zoechat.com', handler: { file: './zoechatweb/css/animate.css' } },
    { method: 'GET', path: '/api/doc.json',  handler: { file: './routes/doc.json' },config: {cors: true} },
    
    { method: 'GET', path: '/apidoc', handler: { file: {path: './doc/dist/index.html'} },config: {cors: true} },
    { method: 'GET', path: '/swagger-ui.css', handler: { file: {path: './doc/dist/swagger-ui.css'} },config: {cors: true} },
    { method: 'GET', path: '/swagger-ui-bundle.js', handler: { file: {path: './doc/dist/swagger-ui-bundle.js'} },config: {cors: true} },
    { method: 'GET', path: '/swagger-ui-standalone-preset.js', handler: { file: {path: './doc/dist/swagger-ui-standalone-preset.js'} },config: {cors: true} },
    // { method: 'GET', path: '/swagger-ui-bundle.js', handler: { file: {path: './doc/dist/swagger-ui-bundle.js'} },config: {cors: true} },

    { method: 'GET', path: '/', vhost: 'admin.zoechat.com', handler: {file: './admin/index.html'} },
    { method: 'GET', path: '/angular/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/angular'} } },
    { method: 'GET', path: '/controller/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/controller'} } },
    { method: 'GET', path: '/css/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/css'} } },
    { method: 'GET', path: '/plugins/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/plugins'} } },
    { method: 'GET', path: '/images/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/images'} } },
    { method: 'GET', path: '/js/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/js'} } },
    { method: 'GET', path: '/views/{params*}', vhost: 'admin.zoechat.com', handler: { directory: {path: './admin/views'} } },
    { method: 'GET', path: '/login.html', vhost: 'admin.zoechat.com', handler: { file: './admin/login.html' } },




  ]);

  // Start the server
  server.start((err) => {
    console.log('Server running at:', server.info.uri);
   //  require('./lib/chat').init(server.listener, function(){
// 			// console.log('REDISCLOUD_URL:', process.env.REDISCLOUD_URL);
// 			console.log('Feeling Chatty?', 'listening on: http://127.0.0.1:' + process.env.PORT);
// 		});
  });
  
 
});




