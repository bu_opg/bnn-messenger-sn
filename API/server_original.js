'use strict';


var Hapi = require('hapi');
const mongojs = require('mongojs');
const hostIP = '172.31.11.58'; //Your Server's IP
const mongouser = 'mongodb'; //Your Mongo username
const mongopassword = ''; //Your Mongo Password
const dbName = 'zoechatplus'; //Your Database Name

var Path = require('path');
var connectionString = mongouser.concat(':').concat(mongopassword).concat('//localhost:27017/').concat(dbName);
// Create a server with a host and port
var server = new Hapi.Server();
server.connection({
	host: hostIP,
    port: 3000
});

var io = require('socket.io')(server.listener);
//Connect to a db
// server.app.db = mongojs('zoechatplus', ['User']);  //<--- Added
server.app.db = mongojs(connectionString, ['User']);
const Good = require('good');
/*

  require('./routes/chat'),
  require('inert'),
  require('hapi-error')
*/
//Load plugins and start server
server.register([require('inert'),require('./routes/user'),require('./routes/api'),require('./routes/admin'),require('hapi-error'), require('vision')], (err) => {

  if (err) {
    throw err;
  }

  register:Good,
  {
    options: {
        reporters: {
            console: [
                {
                    module: 'good-squeeze',
                    name: 'Squeeze',
                    args: [{response: '*', log: '*'}]
                },
                {module: 'good-console'},
                'stdout'
            ]
        }
    }
}

 server.views({
    engines: {
      html: require('handlebars') // or Jade or Riot or React etc. 
    },
    path: Path.resolve(__dirname, './')
  });

  server.route({
    method: 'GET',
    path: '/trial',
    handler: function (request, reply) {
        reply('Hello, world!');
    }
});
  server.route([
    // switch these two routes for a /static handler?
    { method: 'GET', path: '/css/animate.css',  handler: { file: './zoechatweb/css/animate.css' } },
    { method: 'GET', path: '/api/doc.json',  handler: { file: './routes/doc.json' },config: {cors: true} },
    
    { method: 'GET', path: '/apidoc', handler: { file: {path: './doc/dist/index.html'} },config: {cors: true} },
    { method: 'GET', path: '/swagger-ui.css', handler: { file: {path: './doc/dist/swagger-ui.css'} },config: {cors: true} },
    { method: 'GET', path: '/swagger-ui-bundle.js', handler: { file: {path: './doc/dist/swagger-ui-bundle.js'} },config: {cors: true} },
    { method: 'GET', path: '/swagger-ui-standalone-preset.js', handler: { file: {path: './doc/dist/swagger-ui-standalone-preset.js'} },config: {cors: true} },
    // { method: 'GET', path: '/swagger-ui-bundle.js', handler: { file: {path: './doc/dist/swagger-ui-bundle.js'} },config: {cors: true} },


    { method: 'GET', path: '/admin',  handler: {file: './admin/index.html'} },
    { method: 'GET', path: '/angular/{params*}', handler: { directory: {path: './admin/angular'} } },
    { method: 'GET', path: '/controller/{params*}',  handler: { directory: {path: './admin/controller'} } },
    { method: 'GET', path: '/css/{params*}',  handler: { directory: {path: './admin/css'} } },
    { method: 'GET', path: '/plugins/{params*}', handler: { directory: {path: './admin/plugins'} } },
    { method: 'GET', path: '/images/{params*}',  handler: { directory: {path: './admin/images'} } },
    { method: 'GET', path: '/js/{params*}',  handler: { directory: {path: './admin/js'} } },
    { method: 'GET', path: '/views/{params*}',handler: { directory: {path: './admin/views'} } },
    { method: 'GET', path: '/login.html', handler: { file: './admin/login.html' } },




  ]);

  // Start the server
  server.start((err) => {
    console.log('Server running at:', server.info.uri);
   //  require('./lib/chat').init(server.listener, function(){
// 			// console.log('REDISCLOUD_URL:', process.env.REDISCLOUD_URL);
// 			console.log('Feeling Chatty?', 'listening on: http://127.0.0.1:' + process.env.PORT);
// 		});
  });
  
 
});




