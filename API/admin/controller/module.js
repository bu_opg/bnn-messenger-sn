var app = angular.module("ChatServices",[]);

var baseUrl = "http://18.228.84.198:3000/admin/"

app.factory('user',function($http){

	var obj = {};
	obj.userlist = function(){
		$http.get(baseUrl+"userList")
    		.then(function(response) {

    	});
	}

	obj.userdetails = function(data){

		var id = {"id" : data };
		console.log(id);

		$http.post({
					url 				: baseUrl+"userProfile",
					method				: 'POST',
					data				: id
				}).then(function(response){
					console.log(response.data);
				});
		}

		return obj;

});

function ConverToFormData (data, headersGetter) {
    var formData = new FormData();
    angular.forEach(data, function (value, key) {
        formData.append(key, value);
    });
    var headers = headersGetter();
    delete headers['Content-Type'];
    return formData;
}