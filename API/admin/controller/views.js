var app = angular.module("BNN", ["ngRoute",'ChatCtrl']);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/dashboard.html",
        controller : "dashboardCtrl"
    })
    .when("/user", {
        templateUrl : "views/user.html",
        controller : "userCtrl"
    })
    .when("/group", {
        templateUrl : "views/group.html",
        controller : "groupCtrl"
    })
    .when("/userdetails", {
        templateUrl : "views/userdetails.html",
        controller : "UserDetailsCtrl"
    })
    .when("/groupdetails", {
        templateUrl : "views/groupdetails.html",
        controller : "GroupDetailsCtrl"
    })
    .when("/download", {
        templateUrl : "views/download.html"
    })
    .when("/logout",{
        templateUrl : "login.html",
        controller : "logoutCtrl"
    })
    .otherwise({
        templateUrl : "login.html",
        controller : "logoutCtrl"
    });;
});
