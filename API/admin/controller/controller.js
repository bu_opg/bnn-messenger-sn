var app = angular.module("ChatCtrl",['datatables','ChatServices']);

var baseUrl = "http://18.228.84.198:3000/admin/";

function reverseString(str) {
    // Step 1. Use the split() method to return a new array
    var splitString = str.split(""); // var splitString = "hello".split("");
    // ["h", "e", "l", "l", "o"]
 
    // Step 2. Use the reverse() method to reverse the new created array
    var reverseArray = splitString.reverse(); // var reverseArray = ["h", "e", "l", "l", "o"].reverse();
    // ["o", "l", "l", "e", "h"]
 
    // Step 3. Use the join() method to join all elements of the array into a string
    var joinArray = reverseArray.join(""); // var joinArray = ["o", "l", "l", "e", "h"].join("");
    // "olleh"
    
    //Step 4. Return the reversed string
    return joinArray; // "olleh"
}

app.filter('mobilehide', function () {
   
   return function(text){

   		if(!text){
   			return text;
   		}

   		return text.replace(/\d{4}$/, 'XXXX');

   }
});

app.service('authentication',function(){
    
  this.auth = function(){

      var access_token = sessionStorage.getItem('access_token');
      if(access_token){
        var token = access_token.split('.');

        // if(reverseString(token[0]) == token[1]){
          return true;
        // }
        // else{

          // var auth_stauts = authentication.auth(access_token);

          // window.location.href = "http://admin.chat.com/login.html";
        // }
      }
      else
      {
        window.location.href = "http://18.228.84.198:3000/login.html";
      }
    }
});

app.filter('timeconvert', function(){

	return function(timer){
		if(! timer){
			return false;
		}
		var date = new Date(timer);

		var time = date.toString("dd/MM/yyyy HH:MM:ss");

		console.log(time);

		return time;
	}

});

app.controller('dashboardCtrl',function($scope,$http, authentication){

  authentication.auth();

	$scope.listuser = function(){
        $scope.preloader = true;
        $http.get(baseUrl+"dashBoard")
    	.then(function(response) {
    		
        	var data = response.data;

        	$scope.android 		= data.androidUsers;
        	$scope.ios 			= data.iosUsers;
        	$scope.user 		= data.userCount;
        	$scope.group 		= data.groupCount;
        	$scope.message 		= data.totalMessageCount;
        	$scope.textmessage 	= data.textMessageCount;
        	$scope.imagemessage = data.imageMessageCount;
        	$scope.videomessage = data.videoMessageCount;
        	$scope.audiomessage = data.audioMessageCount;
        	$scope.location 	= data.locationMessageCount;
        	$scope.sticker 		= data.stickerMessageCount;
        	$scope.contact 		= data.contactMessageCount;
          $scope.preloader = false;
    	});
   	}
   	$scope.listuser();
});

app.controller('userCtrl',function($scope,$http,authentication){
  authentication.auth();
   $scope.listuser = function(){
        $scope.preloader = true;
        $http.get(baseUrl+"userList")
    .then(function(response) {
        $scope.userlistdata = response.data;
        $scope.preloader = false;
    });
   }
   
   $scope.listuser();
   
   $scope.userprofile = function(data){
      $scope.preloader = true;
   		$scope.name = data;
   		sessionStorage.setItem("user_id", data);
      $scope.preloader = false;

   }
});

app.controller('UserDetailsCtrl',function($scope,$http, authentication){

	authentication.auth();


	$scope.userdetail = function() {
		var id = sessionStorage.getItem("user_id");
		var data = {"id": id};
	  $scope.preloader = true;
		$http.post(baseUrl+"userProfile",data)
	    .then(function(response) {
	    	var userinfo = response.data;
	        $scope.userdetails = userinfo.userdetails;
	        $scope.frndlist = userinfo.friendsList;
	        $scope.groupList = userinfo.groupList;
           $scope.preloader = false;
	    });
	}

	$scope.userdetail();

   $scope.userprofile = function(data){
   		$scope.name = data;
   		sessionStorage.setItem("user_id", data);
   		$scope.userdetail();
   }

   $scope.groupprofile = function(data){
   		$scope.name = data;
   		sessionStorage.setItem("group_id", data);
   }

});

app.controller('groupCtrl',function($scope,$http, authentication){

  authentication.auth();

	$scope.grouplist = function(){
         $scope.preloader = true;
        $http.get(baseUrl+"groupList")
    	.then(function(response) {
        	$scope.group = response.data;
           $scope.preloader = false;
    	});
   	}
   	$scope.grouplist();

   	$scope.groupprofile = function(data){
   		$scope.name = data;
   		sessionStorage.setItem("group_id", data);
   }
});

app.controller('GroupDetailsCtrl', function($scope,$http, authentication){

  authentication.auth();

	$scope.groupdetail = function(){
		var id = sessionStorage.getItem("group_id");
		var data = {"id": id};
     $scope.preloader = true;
		$http.post(baseUrl+"groupProfile",data)
	    .then(function(response) {
	    	$scope.groupinfo = response.data.groupDetails;
	    	$scope.userdetails = response.data.userDetails;
         $scope.preloader = false;
	    });	
	}

	$scope.groupdetail();

	$scope.userprofile = function(data){
   		$scope.name = data;
   		sessionStorage.setItem("user_id", data);
   		$scope.userdetail();
   }

   $scope.groupprofile = function(data){
   		$scope.name = data;
   		sessionStorage.setItem("group_id", data);
   		$scope.groupdetail();
   }
});

app.controller('logoutCtrl', function($scope){
    sessionStorage.removeItem("access_token");
    window.location.href = "http://18.228.84.198:3000/login.html";
});