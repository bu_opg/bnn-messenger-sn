
'use strict';

const Boom = require('boom');
const uuid = require('node-uuid');
const Joi = require('joi');
const Inert = require('inert');

var redis = require("redis");
const PushServerKey = 'AAAAbwDWpnk:APA91bGSrM0xkSa9N9TiWaLtVGMIqo-5t0g9fFkXDoB5qYgQ8H4doFRRZS3W5kKDk39jBEREom8rYMzqDry_tAOqRG7oLXY6h4zi1GfNb2sYEYD0XVo7E6EOonCTTpRUCvDiujPrpeTs';
var apn = require('apn');
var path = require('path');

exports.register = function (server, options, next) {
	const db = server.app.db;

	server.route({
		method: 'GET',
		path: '/user',
		handler: function (request, reply) {
			// io.emit('online',{id:'919500834273'});
			// io.emit('news',{id:'919500834273'});
			reply.file("index.html");

		}
	});

	server.route({
		method: 'GET',
		path: '/trial',
		handler: function (request, reply) {
			console.log('run');
			reply('Hello');
			console.log('run');
		}
	});

	server.route({
		method: 'GET',
		path: '/qrcode',
		handler: function (request, reply) {
			var MobileDetect = require('mobile-detect'),
				md = new MobileDetect(request.headers['user-agent']);
			console.log(md.os());
			if (md.os() == "AndroidOS") {
				reply().redirect('https://play.google.com/store/apps/details?id=com.zybertron.chat');
			} else if (md.os() == "iOS") {
				reply().redirect('https://itunes.apple.com/us/app/chat/id1231021028');
			}
			else {
				reply().redirect('https://www.whatsappupdate.com');
			}

		}
	});

	server.route({
		method: 'GET',
		path: '/user/privacy',
		handler: function (request, reply) {

			console.log('i am here');
			// reply.file('index.html');
			reply.file('routes/privacy.html');
		}
	});

	server.route({
		method: 'POST',
		path: '/user/update',
		handler: function (request, reply) {


			const user = request.payload;

			// 								user.name = request.params.name
			// 								user.image = request.params.image
			// 								user.status = request.params.status
			// 								console.log(request.params.name);

			db.User.update({
				_id: user.chatid
			}, {
					$set: user
				}, function (err, result) {

					if (err) {
						console.log(err);
						user.error = true
						reply(user)
					}
					else {
						if (result.n === 0) {
							user.error = true
							user.message = "Not Found"
							// console.log(result);
							reply(user)
						}
						else {
							user.error = false
							// console.log(result);
							reply(user)
						}

					}
				});

		},
		config: {
			validate: {
				payload: {
					name: Joi.string().min(2).max(1000).required().error(new Error('Name Field is missing')),
					image: Joi.string().min(2).max(1000).optional().error(new Error('Image Field is missing')),
					status: Joi.string().min(2).max(1000).required().error(new Error('Status Field is missing')),
					chatid: Joi.string().min(2).max(1000).required().error(new Error('BNN ID is missing'))
				}
			}
		}

	});
	server.route({
		method: 'POST',
		path: '/user/getOTP',
		handler: function (request, reply) {

			const user = request.payload;
			console.log(request.payload);
			// reply(request.payload);
			db.User.findOne({
				mobileNumber: user.mobileNumber
			}, (err, doc) => {
				// reply(doc);

				if (err) {
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (!doc) {
						user.userExists = false;
					}
					else {
						user.userExists = true;
						user.id = doc.id;
					}
					var accountSid = 'AC29688a136501abc1169e4d7c2304ecc2';
					var authToken = 'a680d733a94a30455dc2735e485f0d61';

					//require the Twilio module and create a REST client 
					var client = require('twilio')(accountSid, authToken);
					var otp = Math.floor(Math.random() * 900000) + 100000;
					console.log(otp);
					// var 
					var message = 'OTP for BNN - '.concat(otp);
					console.log(message);
					var toNumber = "+".concat(user.countryCode).concat(user.mobileNumber);
					console.log(toNumber);
					client.messages.create({
						to: toNumber,
						from: "+18305496476",
						body: message,
					}, function (err, message) {

						if (err) {
							console.log(err);
							var res = {};
							res.otp = otp;
							res.error = "true";
							reply(res);
						}
						else {

							console.log(message);

							console.log(message.sid);

							user.otp = otp;
							user.error = "false";
							user.success = true;
							reply(user);
						}
					});
					// const otp = Math.floor(Math.random()*900000) + 100000;
					//          user.otp = otp;
					//          user.success = true;
					//          user.error = "false";
					//          reply(user);

				}
			});


		},
		config: {
			validate: {
				payload: {
					countryCode: Joi.string().max(5).required().error(new Error('countryCode is missing')),
					mobileNumber: Joi.string().min(5).max(10).required().error(new Error('mobileNumber is missing'))
				}
			}
		}
	});
	server.route({
		method: 'POST',
		path: '/user/register',
		handler: function (request, reply) {
			// reply('In');

			const user = request.payload;
			console.log(request.payload);


			var code = request.payload.countryCode;
			var number = request.payload.mobileNumber;
			var uid = code.concat(number);
			user._id = uid;
			var milliseconds = (new Date).getTime();
			user.lastSeen = milliseconds;
			user.statusPrivacy = 'e';
			user.lastSeenPrivacy = 'e';
			user.imagePrivacy = 'e';
			user.isOnline = 0;
			user.isWebOnline = 0;

			user.status = "Hey! I am using BNN!"
			// reply(user);
			db.User.save(user, (err, result) => {
				if (err) {
					err.success = false;
					return reply(err);
				}
				user.success = true;
				user.error = "false";
				reply(user);

			});
			// reply(request.payload);

		},
		config: {
			validate: {
				payload: {
					image: Joi.string().min(2).max(150).optional(),
					mobileNumber: Joi.string().min(5).max(10).required().error(new Error('MobileNumber is missing')),
					countryCode: Joi.string().max(5).required().error(new Error('CountryCode is missing')),
					name: Joi.string().min(1).max(50).required().error(new Error('Name is missing')),
					pushNotificationId: Joi.string().min(2).max(250).optional().error(new Error('Invalid Device Token')),
					voipToken: Joi.string().min(3).max(250).optional().error(new Error('Invalid VOIP Token')),
					os: Joi.string().optional(),//.min(2).max(250).required().error(new Error('Invalid OS')),
					buildVersion: Joi.string().optional()//required().error(new Error('Invalid Build Version'))
				}
			}
		}
	});

	server.route({
		method: 'POST',
		path: '/channel/getChannelParticipants',
		handler: function (request, reply) {
			var response = {};
			db.Channels.findOne({
				channelId: request.payload.channelId
			}, (err, doc) => {
				if (err) {
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (!doc) {
						reply('no length');
					}
					if (doc == null) {
						reply('no length');
					}
					else {
						response.channelDetails = doc;
						db.ChannelParticipants.find({
							channelId: request.payload.channelId
						}, (err, docu) => {

							if (err) {
								return reply(Boom.wrap(err, 'Internal MongoDB error'));
							}
							else {
								if (docu.length === 0) {
									reply('no length');
								}
								else {
									// 	docu.forEach(function(participant) {
									// 	console.log(participant.participantId);
									// });
									response.participants = docu;
									reply(response);
								}

							}
						});
					}

				}

			});



		},
		config: {
			validate: {
				payload: {
					channelId: Joi.string().required().error(new Error('channelId is missing')),
				}
			}
		}
	});

	server.route({
		method: 'POST',
		path: '/group/getGroupParticipants',
		handler: function (request, reply) {
			var response = {};
			db.Groups.findOne({
				groupId: request.payload.groupId
			}, (err, doc) => {
				if (err) {
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (!doc) {
						reply('no length');
					}
					else if (doc == null) {
						reply('no length');
					}
					else {
						response.groupDetails = doc;
						db.GroupParticipants.find({
							groupId: request.payload.groupId
						}, (err, docu) => {

							if (err) {
								return reply(Boom.wrap(err, 'Internal MongoDB error'));
							}
							else {
								if (docu.length === 0) {
									reply('no length');
								}
								else {
									// 	docu.forEach(function(participant) {
									// 	console.log(participant.participantId);
									// });
									response.participants = docu;
									reply(response);
								}

							}
						});
					}

				}

			});



		},
		config: {
			validate: {
				payload: {
					groupId: Joi.string().required().error(new Error('groupId is missing')),
				}
			}
		}
	});

	server.route({
		method: 'POST',
		path: '/group/updateName',
		handler: function (request, reply) {

			var groupId = request.payload.groupId;
			var name = request.payload.groupName;
			var timestamp = (new Date).getTime();

			var updatedDetails = {};
			updatedDetails.name = name;
			updatedDetails.lastEditedBy = request.payload.from;
			updatedDetails.lastEditedAt = timestamp;
			updatedDetails.lastEdit = 'name';
			db.Groups.update({
				groupId: groupId
			}, {
					$set: updatedDetails
				}, function (err, result) {

					if (err) {
						console.log(err);
						var response = {};
						response.error = true
						reply(response)
					}
					else {
						if (result.n === 0) {
							var response = {};
							response.error = true
							response.message = "Not Found"
							// console.log(result);
							reply(response)
						}
						else {
							var response = {};
							response.error = false
							// console.log(result);
							reply(response)
						}

					}
				});

		},
		config: {
			validate: {
				payload: {
					from: Joi.string().min(5).required().error(new Error('From BNN ID is missing')),
					groupImage: Joi.string().optional(),
					groupName: Joi.string().min(1).max(50).required().error(new Error('Group Name is missing')),
					groupId: Joi.string().min(2).max(250).required().error(new Error('Group Id is missing')),
				}
			}
		}

	});


	server.route({
		method: 'POST',
		path: '/group/updateImage',
		handler: function (request, reply) {

			var groupId = request.payload.groupId;
			var image = request.payload.groupImage;

			var timestamp = (new Date).getTime();

			var updatedDetails = {};
			updatedDetails.image = image;
			updatedDetails.lastEditedBy = request.payload.from;
			updatedDetails.lastEditedAt = timestamp;
			updatedDetails.lastEdit = 'image';
			db.Groups.update({
				groupId: groupId
			}, {
					$set: updatedDetails
				}, function (err, result) {

					if (err) {
						console.log(err);
						var response = {};
						response.error = true
						reply(response)
					}
					else {
						if (result.n === 0) {
							var response = {};
							response.error = true
							response.message = "Not Found"
							// console.log(result);
							reply(response)
						}
						else {
							var response = {};
							response.error = false
							// console.log(result);
							reply(response)
						}

					}
				});

		},
		config: {
			validate: {
				payload: {
					from: Joi.string().min(5).required().error(new Error('From BNN ID is missing')),
					groupImage: Joi.string().required().error(new Error('Image Field is missing')),
					groupId: Joi.string().min(2).max(250).required().error(new Error('Group Id is missing')),
				}
			}
		}

	});

	server.route({
		method: 'POST',
		path: '/group/addParticipants',
		handler: function (request, reply) {


			var timestamp = (new Date).getTime();
			var groupDetails = {};
			groupDetails.name = request.payload.groupName;
			groupDetails.image = request.payload.groupImage;
			groupDetails.groupId = request.payload.groupId;
			groupDetails.createdBy = request.payload.from;
			groupDetails.createdAt = timestamp;
			db.Groups.findOne({
				groupId: request.payload.groupId
			}, (err, doc) => {
				if (err) {
					var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (doc) {
						var participants = JSON.parse(request.payload.participants);
						var count = 0;
						participants.forEach(function (participant) {
							var pid = participant.participantId;

							db.GroupParticipants.findOne({
								participantId: pid
							}, (err, doc) => {
								if (err) {
									var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
									return reply(Boom.wrap(err, 'Internal MongoDB error'));
								}
								else {
									if (!doc) {
										var participantDetails = {};
										//var timestamp = (new Date).getTime();
										participantDetails.participantId = pid;
										participantDetails.groupId = request.payload.groupId;
										participantDetails.joinedAt = timestamp;
										participantDetails.addedBy = request.payload.from;
										participantDetails.isAdmin = 0;
										db.GroupParticipants.save(participantDetails, (err, result) => {
											if (err) {
												err.success = false;
												return reply(err);
											}

											count++;
											if (count === participants.length) {
												var response = {};
												response.error = "false";
												response.success = true;

												var message = {};

												var timestamp = (new Date).getTime();

												message.contentType = 'GroupAdd';
												message.content = 'You are added to this group';
												message.time = timestamp;


												sendGroupMessage(request.payload.groupId, message, request.payload.from);

												reply(response);
											}
										});
									}

								}
							});
						});
					}
					else {
						var response = {};
						response.error = "true";
						response.success = false;
						response.message = 'A group with the group ID not found';
						reply(response);
					}
				}
			});


		},
		config: {
			validate: {
				payload: {
					from: Joi.string().min(5).required().error(new Error('From BNN ID is missing')),
					groupId: Joi.string().min(2).max(250).required().error(new Error('Group Id is missing')),
					participants: Joi.string().required().error(new Error('No Participants found'))
				}
			}
		}
	});


	server.route({
		method: 'POST',
		path: '/user/syncContacts',
		handler: function (request, reply) {
			const user = request.payload;
			var showInContacts = false;
			var newContacts = [];
			var response = {};
			var contacts = JSON.parse(user.contacts);
			var count = 0;

			contacts.forEach(function (contact) {
				var number = contact.mobileNumber;
				var contactName = contact.contactName;
				var isBlocked = 0;
				var status = 1;

				db.User.findOne({
					_id: number
				}, (err, doc) => {
					if (err) {
						var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
						return reply(Boom.wrap(err, 'Internal MongoDB error'));
					}
					else {
						if (!doc) {
							showInContacts = false;
							newContacts.push({ mobileNumber: number, showInContactsPage: showInContacts, contactname: contactName });
						}
						else {
							showInContacts = true;
							newContacts.push({ mobileNumber: number, showInContactsPage: showInContacts, name: doc.name, chatid: doc._id, countryCode: doc.countryCode, image: doc.image, contactname: contactName, status: doc.status });
							var toStore = {};
							toStore.from = request.payload.from;
							toStore.to = doc._id;
							toStore.name = contactName;
							toStore.isBlocked = isBlocked;
							toStore.status = status;
							var timestamp = (new Date).getTime();
							toStore.createdAt = timestamp;

							db.UserFriends.update(
								{ from: request.payload.from, to: doc._id },
								{ $setOnInsert: toStore },
								{ upsert: true },
								function (err, numAffected) {
								}
							);

						}
						count++;
						if (count == contacts.length) {
							response.error = "false";
							response.success = true;
							response.contacts = newContacts;
							reply(response);
						}
					}
				});
			});
		},
		config: {
			validate: {
				payload: Joi.object({
					contacts: Joi.string().min(5).optional(),
					from: Joi.string().required().min(4).error(new Error('From ID is missing'))
				}).required().min(1).error(new Error('No new contacts to sync'))
			}
		}
	});

	server.route({
		method: 'POST',
		path: '/user/acceptCall',
		handler: function (request, reply) {


			var zid = request.payload.to;
			var channelid = request.payload.channelId;
			console.log('accept call');
			console.log(zid);
			var fromid = request.payload.from;
			db.User.findOne({
				_id: zid
			}, (err, doc) => {

				if (err) {
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (doc) {

						var FCM = require('fcm-node');

						var serverKey = PushServerKey;
						var fcm = new FCM(serverKey);

						var timestmp = (new Date).getTime();

						var pushType = 'acceptCall';

						var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
							to: doc.pushNotificationId,


							data: {  //you can send only notification or only data(or include both)
								channel_id: channelid,
								fromId: fromid,
								pushType: pushType
							}
						};

						console.log(message);
						fcm.send(message, function (err, response) {
							if (err) {
								var res = {};
								res.error = "true";
								console.log("Something has gone wrong!");
								console.log(err);
								reply(res);
							} else {
								var res = {};
								res.error = "false";
								reply(res);
								// console.log("Successfully sent with response: ", response);
							}
						});

					}
					else {
						// console.log('error');
						var res = {};
						res.error = "true";
						res.message = "User not found";
						reply(res);
					}

				}
			});


		},
		config: {
			validate: {
				payload: {
					title: Joi.string().min(2).max(1000).optional().error(new Error('Title is missing')),
					body: Joi.string().min(2).max(1000).optional().error(new Error('Device ID is missing')),
					from: Joi.string().min(3).required().error(new Error('From BNN ID is missing')),
					to: Joi.string().min(3).required().error(new Error('To BNN ID is missing')),
					channelId: Joi.string().min(3).required().error(new Error('Channel ID is missing')),
				}
			}
		}
	});

	server.route({
		method: 'POST',
		path: '/user/endCall',
		handler: function (request, reply) {
			console.log('Request');
			console.log(request);
			var zid = request.payload.to;
			var fromid = request.payload.from;
			db.User.findOne({
				_id: zid
			}, (err, doc) => {

				if (err) {
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (doc) {

						var FCM = require('fcm-node');

						var serverKey = PushServerKey;
						var fcm = new FCM(serverKey);

						var timestmp = (new Date).getTime();

						var pushType = request.payload.pushType;

						var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
							to: doc.pushNotificationId,
							data: {  //you can send only notification or only data(or include both)
								fromId: fromid,
								pushType: pushType
							},
							content_available: true
						};

						console.log(message);
						fcm.send(message, function (err, response) {
							if (err) {
								var res = {};
								res.error = "true";
								console.log("Something has gone wrong!");
								console.log(err);
								reply(res);
							} else {
								var res = {};
								res.error = "false";
								reply(res);
								console.log("Successfully sent with response: ", response);
							}
						});

					}
					else {
						console.log('error');
						var res = {};
						res.error = "true";
						res.message = "User not found";
						reply(res);
					}

				}
			});


		},
		config: {
			validate: {
				payload: {
					from: Joi.string().min(3).required().error(new Error('From BNN ID is missing')),
					to: Joi.string().min(3).required().error(new Error('To BNN ID is missing')),
					pushType: Joi.string().min(3).required().error(new Error('pushType is missing'))
				}
			}
		}
	});



	server.route({
		method: 'POST',
		path: '/user/voiceCall',
		handler: function (request, reply) {


			var zid = request.payload.to;
			var fromid = request.payload.from;
			var isVidCall = request.payload.isVideoCall;

			db.User.findOne({
				_id: zid
			}, (err, doc) => {

				if (err) {
					return reply(Boom.wrap(err, 'Internal MongoDB error'));
				}
				else {
					if (doc) {

						if (doc.os) {

							if (doc.os == 'ios') {
								let tokens = [doc.voipToken];

								let service = new apn.Provider({
									cert: "/home/ubuntu/BNN_api/routes/cert/Coworker_VOIP.pem",
									key: "/home/ubuntu/BNN_api/routes/cert/Coworker_VOIP.pem",
								});

								if (isVidCall) {
									pushType = 'videocall';
								}
								else {
									pushType = 'voicecall';
								}

								let payloadData = {
									channel_id: request.payload.channelId,
									fromId: fromid,
									pushType: pushType,
									callType: 'IncomingCall'
								};
								let note = new apn.Notification({
									alert: "Breaking News: I just sent my first Push Notification",
									title: "title",
									body: "body",
									payload: payloadData
								});

								service.send(note, tokens).then(result => {
									console.log("sent:", result.sent.length);
									console.log('RESULT :', result);
									if (result.sent.length > 0) {
										var res = {};
										res.error = "false";
										console.log("Successfully sent with response: ", result);
										reply(res);
									}
									else {
										var res = {};
										res.error = "true";
										console.log("Something has gone wrong!");
										console.log(err);
										reply(res);
									}
									console.log("failed:", result.failed.length);
									console.log(result.failed);
								});
							}
							else {
								var FCM = require('fcm-node');

								var serverKey = PushServerKey;
								var fcm = new FCM(serverKey);

								var timestmp = (new Date).getTime();

								var pushType = '';
								if (isVidCall) {
									pushType = 'videocall';
								}
								else {
									pushType = 'voicecall';
								}
								var callJSONmsg = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
									from: fromid,
									timestamp: timestmp,
									isVideoCall: isVidCall,
									cacheType: 'call'
								};

								console.log(callJSONmsg);
								var client = redis.createClient();
								const uuidv1 = require('uuid/v1');

								var uniqueid = uuidv1();
								client.hset(zid, uniqueid, JSON.stringify(callJSONmsg), redis.print);


								var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
									to: doc.pushNotificationId,

									// notification: {
									//     title: request.payload.from, 
									// 				body: "Incoming Call" 
									// },

									data: {  //you can send only notification or only data(or include both)
										channel_id: request.payload.channelId,
										fromId: request.payload.from,
										pushType: pushType,
										callType: 'IncomingCall'
									},
									content_available: true,
									time_to_live: 10

								};




								console.log(message);
								fcm.send(message, function (err, response) {
									if (err) {
										var res = {};
										res.error = "true";
										console.log("Something has gone wrong!");
										console.log(err);
										reply(res);
									} else {
										var res = {};
										res.error = "false";
										reply(res);
										console.log("Successfully sent with response: ", response);
									}
								});

							}
						}
						else {
							var FCM = require('fcm-node');

							var serverKey = PushServerKey;
							var fcm = new FCM(serverKey);

							var timestmp = (new Date).getTime();

							var pushType = '';
							if (isVidCall) {
								pushType = 'videocall';
							}
							else {
								pushType = 'voicecall';
							}
							var callJSONmsg = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								from: fromid,
								timestamp: timestmp,
								isVideoCall: isVidCall,
								cacheType: 'call'
							};

							console.log(callJSONmsg);
							var client = redis.createClient();
							// client.rpush(zid,JSON.stringify(callJSONmsg));
							const uuidv1 = require('uuid/v1');

							var uniqueid = uuidv1();
							client.hset(zid, uniqueid, JSON.stringify(callJSONmsg), redis.print);

							var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								to: doc.pushNotificationId,

								// notification: {
								//     title: request.payload.from, 
								// 				body: "Incoming Call" 
								// },

								data: {  //you can send only notification or only data(or include both)
									channel_id: request.payload.channelId,
									fromId: request.payload.from,
									pushType: pushType,
									callType: 'IncomingCall'
								},
								content_available: true

							};




							console.log(message);
							fcm.send(message, function (err, response) {
								if (err) {
									var res = {};
									res.error = "true";
									console.log("Something has gone wrong!");
									console.log(err);
									reply(res);
								} else {
									var res = {};
									res.error = "false";
									reply(res);
									console.log("Successfully sent with response: ", response);
								}
							});
						}


					}
					else {
						console.log('error');
						var res = {};
						res.error = "true";
						res.message = "User not found";
						reply(res);
					}

				}
			});


		},
		config: {
			validate: {
				payload: {
					title: Joi.string().min(2).max(1000).optional().error(new Error('Title is missing')),
					body: Joi.string().min(2).max(1000).optional().error(new Error('Device ID is missing')),
					from: Joi.string().min(3).required().error(new Error('From BNN ID is missing')),
					to: Joi.string().min(3).required().error(new Error('To BNN ID is missing')),
					channelId: Joi.string().min(3).required().error(new Error('Channel ID is missing')),
					isVideoCall: Joi.boolean().required().error(new Error('isVideoCall is missing'))
				}
			}
		}

	});


	server.route({
		method: 'POST',
		path: '/user/updateRecoveryDetails',
		handler: function (request, reply) {

			var updatedDetails = {};

			updatedDetails.recoveryEmail = request.payload.email;
			updatedDetails.recoveryPhone = request.payload.phone;


			console.log(updatedDetails);
			var userId = request.payload.id;
			db.User.update({
				_id: userId
			}, {
					$set: updatedDetails
				}, function (err, result) {
					if (err) {
						reply({ error: 'true' });
					}
					if (result.n === 0) {
						var response = {};
						response.error = true
						response.message = "Not Found"
						reply(response)
					}
					else {
						var response = {};
						response.error = false
						reply(response)
					}
				});
		},
		config: {
			validate: {
				payload: {
					email: Joi.string().min(2).max(1000).optional().error(new Error('Invalid Email')),
					phone: Joi.string().min(4).max(14).optional().error(new Error('Invalid Phone')),
					id: Joi.string().min(3).required().error(new Error('BNN ID is missing'))
				}
			}
		}
	});
	server.route({
		method: 'POST',
		path: '/user/updatePrivacySettings',
		handler: function (request, reply) {


			var key = request.payload.key;
			var value = request.payload.value;
			var updatedDetails = {};
			updatedDetails[key] = value;
			console.log(updatedDetails);
			var userId = request.payload.id;
			db.User.update({
				_id: userId
			}, {
					$set: updatedDetails
				}, function (err, result) {
					if (err) {
						reply({ error: 'true' });
					}
					if (result.n === 0) {
						var response = {};
						response.error = true
						response.message = "Not Found"
						reply(response)
					}
					else {
						var response = {};
						response.error = false
						reply(response)
					}
				});
		},
		config: {
			validate: {
				payload: {
					key: Joi.string().min(2).max(1000).optional().error(new Error('Invalid Key')),
					value: Joi.string().max(1).optional().error(new Error('Invalid value for Key')),
					id: Joi.string().min(3).required().error(new Error('BNN ID is missing'))
				}
			}
		}
	});


	server.route({
		method: 'POST',
		path: '/user/checkRecoveryDetails',
		handler: function (request, reply) {
			var userId = request.payload.id;
			db.User.find({ _id: userId, "recoveryEmail": { "$exists": true } }, (err, doc) => {
				if (doc.length == 0) {

					var res = {};
					res.error = "true";
					reply(res);
				} else {
					var res = {};
					res.error = "false";
					reply(res);
				}
			});

		},
		config: {
			validate: {
				payload: {
					id: Joi.string().min(3).required().error(new Error('BNN ID is missing'))
				}
			}
		}
	});

	server.route({
		method: 'POST',
		path: '/user/requestOtp',
		handler: function (request, reply) {
			var userId = request.payload.id;
			var toNumber = "+".concat(userId);
			db.User.find({ _id: userId }, (err, doc) => {
				if (doc.length == 0) {
					var res = {};
					res.error = "true";
					reply(res);
				} else {

					var accountSid = 'AC29688a136501abc1169e4d7c2304ecc2';
					var authToken = 'a680d733a94a30455dc2735e485f0d61';

					//require the Twilio module and create a REST client 
					var client = require('twilio')(accountSid, authToken);
					var otp = Math.floor(Math.random() * 900000) + 100000;
					console.log(otp);
					client.messages.create({
						to: toNumber,
						from: "+18305496476",
						body: otp,
					}, function (err, message) {

						if (err) {
							console.log(err);
							var res = {};
							res.error = "true";
							reply(res);
						}
						else {


							console.log(message.sid);

							var res = {};
							var otp = Math.floor(Math.random() * 900000) + 100000;
							console.log(otp);
							res.otp = otp;
							res.error = "false";
							reply(res);
						}
					});


				}
			});

		},
		config: {
			validate: {
				payload: {
					id: Joi.string().min(3).required().error(new Error('BNN ID is missing'))
				}
			}
		}
	});



	server.route({
		method: 'POST',
		path: '/user/voipPush',
		handler: function (request, reply) {

			let tokens = [request.payload.voipToken];

			let service = new apn.Provider({
				cert: "/home/ubuntu/BNN_api/routes/cert/Coworker_VOIP.pem",
				key: "/home/ubuntu/BNN_api/routes/cert/Coworker_VOIP.pem",
			});

			let payloadData = {
				channel_id: "123245",
				fromId: '919500834273',
				pushType: 'videocall',
				callType: 'IncomingCall'
			};
			let note = new apn.Notification({
				alert: "Breaking News: I just sent my first Push Notification",
				title: "title",
				body: "body",
				payload: payloadData
			});

			// The topic is usually the bundle identifier of your application.

			// console.log(`Sending: ${note.compile()} to ${tokens}`);
			service.send(note, tokens).then(result => {
				console.log("sent:", result.sent.length);
				console.log("failed:", result.failed.length);
				console.log(result.failed);
			});


			// For one-shot notification tasks you may wish to shutdown the connection
			// after everything is sent, but only call shutdown if you need your 
			// application to terminate.
			service.shutdown();
		}
	});


	return next();

}

exports.register.attributes = {
	name: 'routes-api'
};

