'use strict';
const Boom = require('boom');
const Joi = require('joi');
var AWS = require('aws-sdk');
var multer = require('multer');
var s3 = new AWS.S3({
    endpoint: 's3.ap-northeast-2.amazonaws.com',
    signatureVersion: 'v4',
    region: 'ap-northeast-2'
} );


exports.register = function (server, options, next) {
    const db = server.app.db;

    server.route({
		method: 'POST',
    	path: '/admin/login',    	
    	handler: function(request,reply){
    		var user = request.payload.username;
    		var pass = request.payload.password;
    		
    		db.Admin.find({
    			username:request.payload.username,   			
    			password:request.payload.password
    		},(err,doc)=>{
    			console.log(doc);
    			if(err)
    			{
					reply({"error":true,"message":"Server Error"});
    			}
    			else{
    				if(doc.length == 0)
    				{
    					reply({"error":true,"message":"Invalid Credentials","doc":doc});
    				}
    				else{
						reply({"error":false,"message":"Login Successful"});
    				}
    			}    			
    		});
    	},
    	config: {
        	cors: true,
            validate: {
                payload: {                    
                     username: Joi.string().required().error(new Error('Username is missing')),                 
                     password: Joi.string().required().error(new Error('Password is missing'))    
                }
            }
        }
    });

    server.route({
		method: 'GET',
    	path: '/admin/dashBoard',
    	handler: function(request,reply){
    		var response = {};
    		db.User.find({}).count(function(err,userCount){
    			response.userCount = userCount;
				db.Groups.find({}).count(function(err,groupCount){
					response.groupCount = groupCount;
					db.ChatMessages.find({}).count(function(err,totalMessageCount){
						response.totalMessageCount = totalMessageCount;
						db.ChatMessages.find({contentType:'text'}).count(function(err,textMessageCount){
							response.textMessageCount = textMessageCount;
								db.ChatMessages.find({contentType:'image'}).count(function(err,imageMessageCount){
									response.imageMessageCount = imageMessageCount;
									db.ChatMessages.find({contentType:'video'}).count(function(err,videoMessageCount){
										response.videoMessageCount = videoMessageCount;
											db.ChatMessages.find({contentType:'audio'}).count(function(err,audioMessageCount){
												response.audioMessageCount = audioMessageCount;
												db.ChatMessages.find({contentType:'location'}).count(function(err,locationMessageCount)
												{
													response.locationMessageCount = locationMessageCount;
													db.ChatMessages.find({contentType:'sticker'}).count(function(err,stickerMessageCount)
													{
															response.stickerMessageCount = stickerMessageCount;
															db.ChatMessages.find({contentType:'contact'}).count(function(err,contactMessageCount)
															{
																response.androidUsers = 746;
																response.iosUsers = 328;
																response.userCount = 1074;
																response.contactMessageCount = contactMessageCount;
																reply(response);
															});	
													});													
												});
											});
									});
							});

						});
						
					});
				});
    		});

    	}
    });
    server.route({
    	method: 'GET',
    	path: '/admin/userList',    	
    	handler: function(request,reply){
    		db.User.find({    			
    		},(err,doc)=>{
    			if(err)
    			{
    				var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
					    	return reply(Boom.wrap(err, 'Internal MongoDB error'));
    			}
    			else{
    				var userList = [];
    				if(doc.length === 0)
    				{
    					console.log('doc not found');
    					reply(doc);
    				}
    				else{
    					var count = 0;
						doc.forEach(function(user) {							
							db.UserFriends.find({from:user._id}).count(function(err,unreadCount){
								user.friendsCount = unreadCount;	
								
								// db.Groups.find({createdBy:user._id}).count(function(err,groupsIOwncount){
								// 	user.groupsIOwn = groupsIOwncount;
									db.GroupParticipants.find({participantId:user._id}).count(function(err,groupCount){
										user.groupCount = groupCount;
										userList.push(user);
										count ++;
										if(count == doc.length)
										{
											reply(userList);
										}
									});
								// });
								
								
							});		
												
						});
						
    				}

    			}
    		})
    	}
    });

	server.route({
		method: 'GET',
		path: '/admin/groupList',		
		handler: function(request,reply)
		{
			db.Groups.find({    			
    		},(err,doc)=>{
    			if(err)
    			{
    				var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
					    	return reply(Boom.wrap(err, 'Internal MongoDB error'));
    			}
    			else{
    				var groupList = [];
    				if(doc.length === 0)
    				{
    					console.log('doc not found');
    					reply(doc);
    				}
    				else{
    					var count = 0;
						doc.forEach(function(group) {							
								
								db.GroupParticipants.find({groupId:group.groupId}).count(function(err,groupcount){
										group.groupCount = groupcount;
										groupList.push(group);
										count ++;
										if(count == doc.length)
										{
											reply(groupList);
										}								
							});		
												
						});
						
    				}

    			}
			});
		}
	});

	server.route({
    	method: 'POST',
    	path: '/admin/myGroups',    	
    	handler: function(request,reply){
    		
    	},config: {
        	cors: true,
            validate: {
                payload: {                    
                     userId: Joi.string().min(5).required().error(new Error('ID is missing')),                     
                }
            }
        }
    });
    server.route({
    	method: 'POST',
    	path: '/admin/groupsIOwn',    	
    	handler: function(request,reply){
    		db.Groups.aggregate([
	  									{ "$match": { "createdBy": request.payload.userId } },
										{ "$lookup": {
											"localField": "createdBy",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
                                        { "$lookup": {
											"localField": "userinfo._id",
											"from": "Groups",
											"foreignField": "createdBy",
											"as": "groupinfo"
											}
										},
                                        { "$unwind": "$groupinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo.name": 1,
                                            "groupinfo":1
										} 
										}
										],(err,doc)=>{
    											if(err)
								    			{
								    				var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
													    	return reply(Boom.wrap(err, 'Internal MongoDB error'));
								    			}
								    			else{
								    				
								    				if(doc.length === 0)
								    				{
								    					console.log('doc not found');
								    					reply(doc);
								    				}
								    				else{
								    					var count = 0;
								    					var groupList = [];
								    					doc.forEach(function(group) {				
								    						var groupDetail = {};
								    						groupDetail.name = group.groupinfo.name;
								    						groupDetail.image = group.groupinfo.image;
								    						groupDetail._id = group.groupinfo._id;
								    						groupDetail.groupId = group.groupinfo.groupId;
								    						groupDetail.createdBy = group.userinfo.name;
								    						groupDetail.createById = group.groupinfo.createdBy;
								    						groupDetail.createdAt = group.groupinfo.createdAt;

																db.GroupParticipants.find({groupId:group.groupinfo.groupId}).count(function(err,groupCount){

																	groupDetail.groupCount = groupCount;
																	groupList.push(groupDetail);
																	count ++;
																	if(count == doc.length)
																	{
																		reply(groupList);
																	}
																});
																
															});	
								    				}
																																
								    			}
    		});
    	},
        config: {
        	cors: true,
            validate: {
                payload: {                    
                     userId: Joi.string().min(5).required().error(new Error('ID is missing')),                     
                }
            }
        }
    });

	server.route({
		method: 'POST',
		path: '/admin/groupMembers',		
		handler:function(request,reply){
			db.GroupParticipants.find({},(err,doc)=>{
				if(err)
				{
					console.log(err);
					reply('err');
				}
				else{
					if(doc.length == 0)
					{
						reply(doc);
					}
					else{
						var members = [];
						var count = 0;
						doc.forEach(function(user){
							db.User.findOne({
								_id:user.participantId
							},(err,participant)=>{
								count++;
								if(err)
								{

								}
								else{
									console.log(participant);
									if(!participant)
									{
										var userDetails = {};
										userDetails.joinedAt = user.joinedAt;
										userDetails.addedBy = user.addedBy;
										// userDetails.name = participant.name;
										// userDetails.image = participant.image;
										// userDetails.id = participant._id;
										// userDetails.status = participant.status;
										// userDetails.os = participant.os;
										// userDetails.countryCode = participant.countryCode;
											
										members.push(participant);

										if(count == doc.length)
										{		

											reply(members);			
										}	
									}
									else{
									}
									console.log('ADDED');										
								}
								
							});
						});
						
					}
				}

			});
		},
        config: {
        	cors: true,
            validate: {
                payload: {                    
                     groupId: Joi.string().required().error(new Error('ID is missing')),                     
                }
            }
        }
	});
	server.route({
		method: 'POST',
		path:'/admin/groupParticipants',		
		handler: function(request,reply){
			db.Groups.aggregate([{ "$match" : { "groupId": request.payload.groupId } },
										{ "$lookup" : {
											"localField" : "groupId",
											"from" : "GroupParticipants",
											"foreignField" : "groupId",
											"as" : "usergroupinfo"
											}
										},
										{ "$unwind": "$usergroupinfo" },
                                        { "$lookup": {
											"localField": "usergroupinfo.participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userpersonalinfo"
											}
										},
                                        { "$unwind": "$userpersonalinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userpersonalinfo": 1,
                                            "usergroupinfo":1
											} 
										}
										],(err,doc)=>{
											console.log(doc);
											if(err)
											{							
												console.log(err);
												reply('err');
											}
											else{
												if(doc.length == 0)
												{							
													reply(doc);
													console.log("LENGTH 0");
												} 
												else{
													reply(doc);
												}
											}
			});
		},
        config: {
        	cors: true,
            validate: {
                payload: {                    
                     groupId: Joi.string().required().error(new Error('groupId is missing')),                     
                }
            }
        }
	});
   
	 server.route({
        method: 'POST',
        path: '/admin/updateProfile',        
        handler: function (request, reply) {
        
        	var details = {};
        	var user = {};
        	if(request.payload.key == 0)
        	{
        		if(request.payload.name){
        			if(request.payload.name.length >0)
        			{
						details.name = request.payload.name;   
	        		}
	        		else{
	        			reply({error:true,message:'Invalid Name'});
	        		}	
        		}
        		else{
	        			reply({error:true,message:'Invalid Name'});
	        	}
				     	
        	}
        	else if(request.payload.key == 1)
        	{
        		if(request.payload.image){

        			if(request.payload.image.length >0)
	        		{
	        			details.image = request.payload.image;        		
	        		}
	        		else{
	        			reply({error:true,message:'Invalid image URL'});
	        		}
				}
				else{
	        			reply({error:true,message:'Invalid image URL'});
	        	}
        	}
        	else if(request.payload.key == 2){
				
				if(request.payload.status){
					if(request.payload.status.length >0)
	        		{
						details.status = request.payload.status;   
	        		}
	        		else{
	        			reply({error:true,message:'Invalid Status'});
	        		}
        		}	
        		else{
					reply({error:true,message:'Invalid Status'});
        		}
        	}
        	
     			db.User.update({
	             	_id: request.payload.id
        		}, {
                	$set: details
		          	}, (err, doc) => {	

	                if (doc.n === 0) {

            			user.error = true	
            			user.message = "Not Found"						
						reply(user)
				    }
				    else{
				        user.error = false						
						reply(user)
				   }
	            });
                
        },
        config: {
        	cors: true,
            validate: {
                payload: {                    
                     id: Joi.string().min(5).required().error(new Error('ID is missing')),
                     key: Joi.required().error(new Error('key is missing')),
                     name: Joi.string().min(2).optional().error(new Error('Invalid name')),
                     image: Joi.string().min(5).optional().error(new Error('Invalid image')),
                     status : Joi.string().min(1).optional().error(new Error('Invalid Status')),
                }
            }
        }
    });
	

	 server.route({
        method: 'POST',
        path: '/admin/userFriends',        
        handler: function (request, reply) {
        	        	
        	
        		db.User.aggregate([
	  				{ "$match": { "_id": request.payload.id } },
					{ "$lookup": {
						"localField": "_id",
						"from": "UserFriends",
						"foreignField": "from",
						"as": "userfriends"
						}
					},
                    { "$unwind": "$userfriends"},
                    { "$lookup": {
						"localField": "userfriends.to",
						"from": "User",
						"foreignField": "_id",
						"as": "userinfo"
						}
					},
					{ "$unwind": "$userinfo" },
					{ "$project": {
						"text": 1,
						"date": 1,
						"userinfo": 1,
                        "userfriends":1
                        }                                                                                
					}
					],(err, doc) => {																					

						if (err) {
							return reply(Boom.wrap(err, 'Internal MongoDB error'));
						}
						else{
							var friends = [];
							doc.forEach(function(friend) {
								var eachFriend ={};
								eachFriend.name = friend.userfriends.name;
								eachFriend.status = friend.userinfo.status;
								eachFriend.image = friend.userinfo.image;
								eachFriend.id = friend.userinfo._id;
								friends.push(eachFriend);
								});
								reply(friends);
							}
					});
                
        },
        config: {
        	cors: true,
            validate: {
                payload: {                    
                     id: Joi.string().min(5).required().error(new Error('ID is missing'))
                }
            }
        }
    });


	server.route({
		method: 'POST',
		path: '/admin/groupProfile',	
		handler: function(request,reply){
			db.Groups.findOne({
				groupId:request.payload.id
			},(err,doc)=>{
				if(err)
				{
					reply({error:true,message:'Group not found'});
				}
				else{
					if(doc)
					{
						var response = {};
						db.GroupParticipants.find({groupId:request.payload.id}).count(function(err,groupCount){
							doc.totalMembers = groupCount;
							db.GroupParticipants.aggregate([{ "$match": { "groupId": request.payload.id } },
                                                    {"$lookup": {
                                                        "localField": "participantId",
                                                        "from": "User",
                                                        "foreignField": "_id",
                                                        "as": "userlist"
                                                        }       
                                                    },
                                                    { "$unwind": "$userlist"}, 
                                                    {"$lookup": {
                                                        "localField": "groupId",
                                                        "from": "GroupParticipants",
                                                        "foreignField": "groupId",
                                                        "as": "groupinfo"
                                                        }       
                                                    },
                                                    { "$unwind": "$groupinfo"},  
                                                    {"$lookup": {
                                                        "localField": "addedBy",
                                                        "from": "User",
                                                        "foreignField": "_id",
                                                        "as": "addedByInfo"
                                                        }       
                                                    },
                                                    { "$unwind": "$addedByInfo"},
                                                    { "$project": {
                                                        "text": 1,
                                                        "date": 1,
                                                        "userlist": 1 ,
                                                        "groupinfo":1,
                                                        "addedByInfo":1
                                                     }                                                                                
                                                  }],(err,groupinfo)=>{
                                                  		response.groupDetails = doc;
                                                  		response.userDetails = groupinfo;
                                                  		reply(response);
                                                  });
						});


					}
					else{
						reply({error:true,message:'Group not found'});
					}
				}
			});
		},
		config: {
        	cors: true,
            validate: {
                payload: {                    
                     id: Joi.string().min(5).required().error(new Error('ID is missing'))
                }
            }
        }
	});
	 server.route({
        method: 'POST',
        path: '/admin/userProfile',        
        handler: function (request, reply) {
        
     			db.User.findOne({
	                _id: request.payload.id
	            }, (err, doc) => {	

	                if (err) {
	                    return reply(Boom.wrap(err, 'Internal MongoDB error'));
	                }
					else{
	                if (!doc) {
	                	reply({error:true,message:'User not found'});
	                }
					else{
						var response = {};
						response.userdetails = doc;
						response.error = false;
						console.log('User profile');
						db.User.aggregate([
	  						{ "$match": { "_id": request.payload.id } },
							{ "$lookup": {
								"localField": "_id",
								"from": "UserFriends",
								"foreignField": "from",
								"as": "userfriends"
								}
							},
		                    { "$unwind": "$userfriends"},
		                    { "$lookup": {
								"localField": "userfriends.to",
								"from": "User",
								"foreignField": "_id",
								"as": "userinfo"
								}
							},
							{ "$unwind": "$userinfo" },
							{ "$project": {
								"text": 1,
								"date": 1,
								"userinfo": 1,
		                        "userfriends":1
		                        }                                                                                
							}
							],(err, doc) => {																							

								if (err) {
									response.friendsList = [];
									db.Groups.find({    			
    									},(err,groups)=>{
							    			if(err)
							    			{
							    				var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
							    				console.log(error);
							    			}
							    			else{
							    				var groupList = [];
							    				if(groups.length === 0)
							    				{
							    					console.log('doc not found');
							    					response.groupList = [];
							    					reply(response);
							    				}
							    				else{
							    					response.groupList = groups;
													reply(response);														
							    				}							

							    			}
										});
								}
								else{
									var friends = [];
									doc.forEach(function(friend) {
										var eachFriend ={};
										eachFriend.name = friend.userfriends.name;
										eachFriend.status = friend.userinfo.status;
										eachFriend.image = friend.userinfo.image;
										eachFriend.id = friend.userinfo._id;
										friends.push(eachFriend);
										});
										response.friendsList = friends;
										db.Groups.find({    			
    									},(err,groups)=>{
							    			if(err)
							    			{
							    				var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
							    				console.log(error);
							    			}
							    			else{
							    				var groupList = [];
							    				if(groups.length === 0)
							    				{
							    					console.log('doc not found');
							    					response.groupList = [];
							    					reply(response);
							    				}
							    				else{
							    					response.groupList = groups;
													reply(response);														
							    				}							

							    			}
										});
									}
							});
	                	}
	                }
	            });
                
        },
        config: {
        	cors: true,
            validate: {
                payload: {                    
                     id: Joi.string().min(5).required().error(new Error('ID is missing'))
                }
            }
        }
    });
           




    return next();
};
exports.register.attributes = {
    name: 'routes-chatadmin'
};