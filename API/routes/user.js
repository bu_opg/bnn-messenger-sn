'use strict';

const Boom = require('boom');
const uuid = require('node-uuid');
const Joi = require('joi');
const Inert = require('inert');

const PushServerKey = 'AAAAbwDWpnk:APA91bGSrM0xkSa9N9TiWaLtVGMIqo-5t0g9fFkXDoB5qYgQ8H4doFRRZS3W5kKDk39jBEREom8rYMzqDry_tAOqRG7oLXY6h4zi1GfNb2sYEYD0XVo7E6EOonCTTpRUCvDiujPrpeTs';

var async = require('async');
var SocketIO = require('socket.io');
var redis = require("redis");
var Jimp = require("jimp");
var AWS = require('aws-sdk');
var s3 = new AWS.S3({
    endpoint: 's3.us-east-2.amazonaws.com',
    signatureVersion: 'v4',
    region: 'us-east-2'
} );
// old
// var s3 = new AWS.S3({
//     endpoint: 's3.ap-northeast-2.amazonaws.com',
//     signatureVersion: 'v4',
//     region: 'ap-northeast-2'
// } );

var apn = require('apn');

exports.register = function (server, options, next) {

    const db = server.app.db;
	var io = require('socket.io')(server.listener);
		io.set("heartbeat interval", 3000);
		io.set("heartbeat timeout", 3000);
		io.on('connection',function(socket){
				console.log('user connected');
				var hs = socket.handshake;
				// When User comes online

			
				
				socket.on('disconnect', function () {
	        		console.log('A socket with sessionID ' + hs.sessionID + ' disconnected!');
	    //     		var milliseconds = (new Date).getTime();
					// var user = {"isOnline":0,"lastSeen":milliseconds};
					
					// // console.log(user);
     //       				db.User.update({
		   //          	    _id: hs.sessionID
     //    		   		 }, {
     //            			$set: user
		   //         		 }, function (err, result) {
	
    	// 		    	        if (err) {
     //            				    console.log(err);
     //        	    			}
					// 			else{
					// 				// console.log(result);
					// 			}
     //        			});
				});

				socket.on('qridemit',function(data)
				{
					// console.log(data);
					var from = data.from;
					var qrcode = data.id;
					socket.broadcast.emit('qrscanid',{code:qrcode,chatid:from});
				});
				
				socket.on('webonline',function(data,callback){
					// console.log(data.id)
					hs.sessionID = data.id;
					var user = {"isWebOnline":1};
					var receiverprefix = 'qrscanid';

					db.User.update({
		            	    _id: data.id
        		   		 }, {
                			$set: user
		           		 }, function (err, result) {
	
    			    	        if (err) {
                				    console.log(err);
            	    			}
								else{
									// console.log(result);
								}
            			});		
				});

				socket.on('weboffline',function(data,callback){
					console.log(data.id);
					hs.sessionID = data.id;
					var milliseconds = (new Date).getTime();
					var user = {"isWebOnline":0,"lastSeen":milliseconds};
					
					console.log(user);
           				db.User.update({
		            	    _id: data.id
        		   		 }, {
                			$set: user
		           		 }, function (err, result) {
	
    			    	        if (err) {
                				    console.log(err);
            	    			}
								else{
									// console.log(result);
								}
            			});	
				});



				socket.on('online',function(data){
					console.log(data.id);
					var user = {"isOnline":1};
					hs.sessionID = data.id;
					// socket.emit('connected','You are now online');
					console.log(user);
					db.User.update({
		            	    _id: data.id
        		   		 }, {
                			$set: user
		           		 }, function (err, result) {
	
    			    	        if (err) {
                				    console.log(err);
            	    			}
								else{
									// console.log(result);
								}
            			});	

					console.log('A socket with sessionID ' + hs.sessionID + ' connected!');
					setTimeout(function() {
						var client = redis.createClient();
						var receiver =  data.id;
						
						client.hgetall(data.id, function (err, obj) {
							if(!err){
								// console.log(obj);
								for (var key in obj) {
 								 if (obj.hasOwnProperty(key)) {
								    console.log(key + " -> " +obj[key]);
								    var deserializedmsg = JSON.parse(obj[key]);
									console.log(deserializedmsg);
								    var receiverprefix;	

									if(deserializedmsg.cacheType == 'chat')
									{
										receiverprefix =receiver.concat(":receiveMessage");									
									}
									else if(deserializedmsg.cacheType == 'call'){
										receiverprefix =receiver.concat(":receiveCall");								
									}
									else if(deserializedmsg.cacheType == 'groupadd'){
										receiverprefix =receiver.concat(":group");									
									}
									else if(deserializedmsg.cacheType == 'channeladd'){
										receiverprefix =receiver.concat(":channel");									
									}
									else if(deserializedmsg.cacheType == 'ack'){
										receiverprefix =receiver.concat(":ack");									
									}
									socket.emit(receiverprefix, deserializedmsg);
									console.log('recteiver = '.concat(receiverprefix));
								  }
								}
							}    							
    						
						});


					// 	client.lrange(receiver, 0, -1, function (err, res) {
						
					// 		if(!err)
					// 		{
					// 		    res.forEach(function(msg) {
					// 				var deserializedmsg = JSON.parse(msg);
					// 				console.log('deserialized');
					// 				console.log(deserializedmsg);
					// 				var receiverprefix;	

					// 				if(deserializedmsg.cacheType == 'chat')
					// 				{
					// 					receiverprefix =receiver.concat(":receiveMessage");									
					// 				}
					// 				else if(deserializedmsg.cacheType == 'call'){
					// 					receiverprefix =receiver.concat(":receiveCall");								
					// 				}
					// 				else if(deserializedmsg.cacheType == 'groupadd'){
					// 					receiverprefix =receiver.concat(":group");									
					// 				}
					// 				else if(deserializedmsg.cacheType == 'channeladd'){
					// 					receiverprefix =receiver.concat(":channel");									
					// 				}
					// 				else if(deserializedmsg.cacheType == 'ack'){
					// 					receiverprefix =receiver.concat(":ack");									
					// 				}
					// 				socket.emit(receiverprefix, deserializedmsg);
					// 				console.log('recteiver = '.concat(receiverprefix));
					// 			});
					// 			client.DEL(receiver);							
					// 		}
					// 		else{
					// 		console.log('redis error lrange');	

					// 		}
					// });
					
                    	

               //  });
            }, 10);

					
           					
				});
				
				// When User goes offline
				socket.on('offline',function(data){
					console.log(data.id);
					var milliseconds = (new Date).getTime();
					var user = {"isOnline":0,"lastSeen":milliseconds};
					
					console.log(user);
           				db.User.update({
		            	    _id: data.id
        		   		 }, {
                			$set: user
		           		 }, function (err, result) {
	
    			    	        if (err) {
                				    console.log(err);
            	    			}
								else{
									// console.log(result);
								}
            			});		
				});


				
				socket.on('typing',function(data){
					var typing = {};
					// console.log(data);
					var receiver =  data.to;
					var sender = data.from;
					typing.to = receiver;
					typing.from = data.from;
					typing.status = data.status;
					
					var roomId = "";					
						if(sender<receiver)
						{
							roomId = sender.concat(receiver);
						}
						else{
							roomId = receiver.concat(sender);
						}						
					typing.roomId = roomId;
					var receiverprefix = receiver.concat(":typing");
					socket.broadcast.emit(receiverprefix, typing);
					
					// console.log('typing receiver = '.concat(receiverprefix));
				});
				


				socket.on('editGroupImage',function(data,callback){
					var groupId = data.groupId;
					var image = data.groupImage;
					var from = data.from;
					var cb = callback;
					var details = {};
					details.image = image;
					details.imageLastUpdatedBy = from;
					db.Groups.update({
		            	    groupId: groupId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
	
    			    	        if (err) {
                				    console.log(err);
                				    details.error = true
                				    cb(false);
            	    			}
								else{
									if (result.n === 0) {
										cb(false);
				            	    }
				            	    else{
				            	    	if(cb)
				            	    	{
				            	    		cb(true);	
				            	    	}
										
										db.GroupParticipants.aggregate([
	  									{ "$match": { "groupId": data.groupId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
												// reply(doc);
														doc.forEach(function(participant) {		
															var message = {};
															message.contentType = 'GroupImageEdit';
															message.content = 'Group Image Edited';
															message.groupId = data.groupId;
															message.groupName = data.groupName;
															message.groupImage  = data.groupImage;
															message.cacheType = 'groupadd';
															var timestamp = (new Date).getTime();
															message.time = timestamp;

															message.from = data.from;
																																												
															var receiverprefix = participant.userinfo._id.concat(":group");
															// console.log('edit group image starts');
															// console.log(receiverprefix);
															// console.log(message);
															// console.log('edit group image ends');
															if(participant.userinfo._id === data.from)
															{

															}
															else{
																// if(!participant.userinfo.isOnline)														
																// {
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																	// client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }
																// else{
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															}
														});

													}
													else{
														console.log('error');
														var res = {};
														res.error = "true";
														res.message = "User not found";
														reply(res);
													}	
												}
										});	
				            	    }
                				    
								}
            			});		
				});
				
				socket.on('deleteFromCache',function(data,callback){
					console.log('deleteFromCache');
					console.log(data);
					var stringified = JSON.stringify(data);
					// var client = redis.createClient();
					// client.lrem(stringified.to,0,data);
				});
				

				socket.on('editGroupName',function(data,callback){
					var groupId = data.groupId;
					var name = data.groupName;
					var from = data.from;
					var cb = callback;
					var details = {};
					details.name = name;
					details.nameLastUpdatedBy = from;
					db.Groups.update({
		            	    groupId: groupId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
									
    			    	        if (err) {
                				    console.log(err);
                				    details.error = true
                				    cb(false);
            	    			}
								else{
									if (result.n === 0) {            			    	    
										cb(false);
				            	    }
				            	    else{
				            	    	if(cb)
				            	    	{
				            	    		cb(true);	
				            	    	}
										db.GroupParticipants.aggregate([
	  									{ "$match": { "groupId": data.groupId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
												// reply(doc);
														doc.forEach(function(participant) {		
															var message = {};
															
															message.contentType = 'GroupNameEdit';
															message.content = 'GroupName edited';
															message.groupId = data.groupId;
															message.groupName = data.groupName;
															message.oldName = data.oldName;
															message.cacheType = 'groupadd';

															var timestamp = (new Date).getTime();
															message.time = timestamp;
															
															message.from = data.from;
																																												
															var receiverprefix = participant.userinfo._id.concat(":group");
															// console.log('edit group name starts');
															// console.log(receiverprefix);
															// console.log(message);
															// console.log(participant.userinfo.isOnline);
															// console.log('edit group name ends');
															if(participant.userinfo._id === data.from)
															{

															}
															else{
																// if(!participant.userinfo.isOnline)														
																// {
																	// console.log('stored in redis');
																	// var client = redis.createClient();																	
																	// client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }
																// else{
																	// console.log('adding group sockt');
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	

																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															}
														});

													}
													else{
														console.log('error');
														var res = {};
														res.error = "true";
														res.message = "User not found";
														reply(res);
													}	
												}
										});									
										
				            	    }
                				    
								}
            			});		
				});
				
				socket.on('removeAdmin',function(data,callback){
					var groupId = data.groupId;
					var participantId = data.participantId;
					var cb = callback;
					var details = {};
					details.isAdmin = 0;
					db.GroupParticipants.update({
		            	    groupId: groupId,
		            	    participantId: participantId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
		           		 		if(err)
		           		 		{
									cb(false);
		           		 		}
		           		 		else{
		           		 			if(result.n === 0){
										cb(false);
		           		 			}	
		           		 			else{
		           		 				if(cb)
		           		 				{
		           		 					cb(true);
		           		 				}
		           		 				db.GroupParticipants.aggregate([
	  										{ "$match": { "groupId": data.groupId } },
											{ "$lookup": {
												"localField": "participantId",
												"from": "User",
												"foreignField": "_id",
												"as": "userinfo"
												}
											},
											{ "$unwind": "$userinfo" },
											{ "$project": {
												"text": 1,
												"date": 1,
												"userinfo": 1,
											} 
											}
											],(err, doc) => {																

												if (err) {
													return reply(Boom.wrap(err, 'Internal MongoDB error'));
												}
												else{
													if (doc) {																                
													// reply(doc);
															doc.forEach(function(participant) {		
																var message = {};
																
																message.contentType = 'GroupRemoveAdmin';
																message.content = 'Group admin';
																message.groupId = data.groupId;
																message.cacheType = 'groupadd';	

																var timestamp = (new Date).getTime();
																message.time = timestamp;
																
																																													
																var receiverprefix = participant.userinfo._id.concat(":group");
																// console.log('exit group name starts');
																// console.log(receiverprefix);
																// console.log(message);
																// console.log(participant.userinfo.isOnline);
																// console.log('exit group name ends');
																// if(!participant.userinfo.isOnline)														
																// {
																	// console.log('stored in redis');
																	// var client = redis.createClient();																	
																	// client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }
																// else{
																	// console.log('adding group sockt');
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															});	

														}
														else{
															console.log('error');
															var res = {};
															res.error = "true";
															res.message = "User not found";
															reply(res);
														}	
													}
											});	

		           		 			}	
		           		 		}
		           		 		
		           		 });
					 
				});


				socket.on('makeAdmin',function(data,callback){
					var groupId = data.groupId;
					var participantId = data.participantId;
					var cb = callback;
					var details = {};
					details.isAdmin = 1;
					details.madeAdminBy = data.from;
					// console.log(data)
					db.GroupParticipants.update({
		            	    groupId: groupId,
		            	    participantId: participantId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
		           		 		if(err)
		           		 		{
									cb(false);
		           		 		}
		           		 		else{
		           		 			if(result.n === 0){
										cb(false);
		           		 			}	
		           		 			else{
		           		 				if(cb)
		           		 				{
		           		 					cb(true);
		           		 				}

		           		 				db.GroupParticipants.aggregate([
	  										{ "$match": { "groupId": data.groupId } },
											{ "$lookup": {
												"localField": "participantId",
												"from": "User",
												"foreignField": "_id",
												"as": "userinfo"
												}
											},
											{ "$unwind": "$userinfo" },
											{ "$project": {
												"text": 1,
												"date": 1,
												"userinfo": 1,
											} 
											}
											],(err, doc) => {																

												if (err) {
													return reply(Boom.wrap(err, 'Internal MongoDB error'));
												}
												else{
													if (doc) {																                
													// reply(doc);
															doc.forEach(function(participant) {		
																var message = {};
																
																message.contentType = 'GroupAddAdmin';
																message.content = 'Group admin';
																message.groupId = data.groupId;
																message.cacheType = 'groupadd';	

																var timestamp = (new Date).getTime();
																message.time = timestamp;
																
																message.from = data.from;
																																													
																var receiverprefix = participant.userinfo._id.concat(":group");
																// console.log('exit group name starts');
																// console.log(receiverprefix);
																// console.log(message);
																// console.log(participant.userinfo.isOnline);
																// console.log('exit group name ends');
																// if(!participant.userinfo.isOnline)														
																// {
																	// console.log('stored in redis');
																	// var client = redis.createClient();																	
																	// client.rpush(participant.userinfo._id,JSON.stringify(message));

																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																// }
																// else{
																	// console.log('adding group sockt');
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															});	

														}
														else{
															console.log('error');
															var res = {};
															res.error = "true";
															res.message = "User not found";
															reply(res);
														}	
													}
											});	

		           		 			}	
		           		 		}
		           		 		
		           		 });
					 
				});
				

				


				socket.on('removeParticipant',function(data,callback){
					var groupId = data.groupId;
					var participantId = data.participantId;
					var cb = callback;
					db.GroupParticipants.remove({
				            groupId: groupId,
				            participantId: participantId
				        }, function (err, result) {				

				            if (err) {
				                cb(false);
				            }				

				            if (result.n === 0) {
				                cb(false);
				            }	
				            else{
				            	cb(true);
				            }		
				            db.User.findOne({
					        	_id: data.participantId
							}, (err, doc) => {																	

									if (err) {
										return reply(Boom.wrap(err, 'Internal MongoDB error'));
									}
									else
									{										
										if (doc) 
										{	
											var message = {};
															
											message.contentType = 'GroupRemoveParticipant';
											message.content = 'Group participant removed';
											message.groupId = data.groupId;
											message.removedId = data.participantId;			
											message.cacheType = 'groupadd';
											var timestamp = (new Date).getTime();
											message.time = timestamp;
																		
											message.from = data.from;
																			
											var part = data.participantId;							

											var receiverprefix = data.participantId.concat(":group");


											// if(!doc.isOnline)														
											// {
											// 	console.log('REMOVED PERSON OFFLINE');
											// 	console.log(receiverprefix);
											// 	var client = redis.createClient();																	
											// 	client.rpush(data.participantId,JSON.stringify(message));
											// }
											// else{
											// 	console.log('REMOVED PERSON ONLINE');
											// 	console.log(receiverprefix);
												const uuidv1 = require('uuid/v1');
																	
												message.uniqueid = uuidv1();
																	
												var client = redis.createClient();																	
												client.hset(data.participantId, message.uniqueid, JSON.stringify(message),redis.print);	
												socket.broadcast.emit(receiverprefix, message);
												socket.emit(receiverprefix, message);				
											// }																								
										}			
									}
								});	
				            db.GroupParticipants.aggregate([
	  									{ "$match": { "groupId": data.groupId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
												// reply(doc);
														doc.forEach(function(participant) {		
															var message = {};
															
															message.contentType = 'GroupRemoveParticipant';
															message.content = 'Group participant removed';
															message.groupId = data.groupId;
															message.removedId = data.participantId;
															message.cacheType = 'groupadd';
															var timestamp = (new Date).getTime();
															message.time = timestamp;
															
															message.from = data.from;
																																												
															var receiverprefix = participant.userinfo._id.concat(":group");
															// console.log('exit group name starts');
															// console.log(receiverprefix);
															// console.log(message);
															// console.log(participant.userinfo.isOnline);
															// console.log('exit group name ends');
															// if(!participant.userinfo.isOnline)														
															// {
																// console.log('stored in redis');
																// var client = redis.createClient();																	
																// client.rpush(participant.userinfo._id,JSON.stringify(message));
															// }
															// else{
																// console.log('adding group sockt');
																const uuidv1 = require('uuid/v1');
																	
																message.uniqueid = uuidv1();
																	
																var client = redis.createClient();																	
																client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																socket.broadcast.emit(receiverprefix, message);
																socket.emit(receiverprefix, message);				
															// }
														});
													}
													else{
														console.log('error');
														var res = {};
														res.error = "true";
														res.message = "User not found";
														reply(res);
													}	
												}
										});	
				            
				        });
				});

				socket.on('exitGroup',function(data,callback){
					var groupId = data.groupId;
					var participantId = data.from;
					var cb = callback;
					db.GroupParticipants.remove({
				            groupId: groupId,
				            participantId: participantId
				        }, function (err, result) {				

				            if (err) {
				                cb(false);
				            }				

				            if (result.n === 0) {
				                cb(false);
				            }	
				            else{
				            	cb(true);
				            }			
				            db.GroupParticipants.aggregate([
	  									{ "$match": { "groupId": data.groupId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
												// reply(doc);
														doc.forEach(function(participant) {		
															var message = {};
															
															message.contentType = 'GroupExit';
															message.content = 'GroupName edited';
															message.groupId = data.groupId;
															message.cacheType = 'groupadd';

															var timestamp = (new Date).getTime();
															message.time = timestamp;
															
															message.from = data.from;
																																												
															var receiverprefix = participant.userinfo._id.concat(":group");
															// console.log('exit group name starts');
															// console.log(receiverprefix);
															// console.log(message);
															// console.log(participant.userinfo.isOnline);
															// console.log('exit group name ends');
															// if(!participant.userinfo.isOnline)														
															// {
																// console.log('stored in redis');
																// var client = redis.createClient();																	
																// client.rpush(participant.userinfo._id,JSON.stringify(message));
															// }
															// else{
																// console.log('adding group sockt');
																const uuidv1 = require('uuid/v1');
																	
																message.uniqueid = uuidv1();
																	
																var client = redis.createClient();																	
																client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																socket.broadcast.emit(receiverprefix, message);
																socket.emit(receiverprefix, message);				
															// }
														});

													}
													else{
														console.log('error');
														var res = {};
														res.error = "true";
														res.message = "User not found";
														reply(res);
													}	
												}
										});	
				            
				        });
				});
				socket.on('addParticipants',function(data,callback){
						var timestamp = (new Date).getTime();
            			var groupDetails = {};
            			// console.log('add participants');
            			// console.log(data);
			            groupDetails.groupId = data.groupId;
			            groupDetails.createdBy = data.from;
						groupDetails.createdAt = timestamp; 
						var cb = callback;
						groupDetails.participantId = data.participantId;
						db.Groups.findOne({
							groupId: data.groupId
			    			}, (err, doc) => {
			    				if (err) {
			    			            	var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
			    			                return reply(Boom.wrap(err, 'Internal MongoDB error'));
			    			            }
										else{
			    			            if (doc) {
			    							var pid = data.participantId;			
			    						
			    			            					var participantDetails = {};
			    			            					//var timestamp = (new Date).getTime();
			    			            					participantDetails.participantId = pid;
			    			            					participantDetails.groupId = data.groupId;
			    			            					participantDetails.joinedAt = timestamp;
			    			            					participantDetails.addedBy = data.from;
			    			            					participantDetails.isAdmin = 0;
															db.GroupParticipants.save(participantDetails,(err,result) =>{
																if (err) {
			                										err.success = false;
			               									     	return reply(err);
			                									}    			
			                												if(cb)
				            	    										{
													            	    		cb(true);	
													            	    	}

			                										db.GroupParticipants.aggregate([
  																{ "$match": { "groupId": data.groupId } },
																{ "$lookup": {
																	"localField": "participantId",
																	"from": "User",
																	"foreignField": "_id",
																	"as": "userinfo"
																	}
																},
																{ "$unwind": "$userinfo" },
																{ "$project": {
																	"text": 1,
																	"date": 1,
																	"userinfo": 1,
																} 
																}
																],(err, doc) => {																					

																	if (err) {
																		return reply(Boom.wrap(err, 'Internal MongoDB error'));
																	}
																	else{
																		if (doc) {																                
																			// reply(doc);
																			
																					doc.forEach(function(participant) {		
																						var message = {};
															
																						var timestamp = (new Date).getTime();									
																						
																						if(participant.userinfo._id === data.participantId){
																							message.contentType = 'GroupAdd';																				
																						}
																						else{
																							message.contentType = 'PartAdd';
																						}
																						message.content = 'You are added to this group';
																						message.time = timestamp;
																						message.groupId = data.groupId;
																						message.groupName = data.groupName;
																						message.groupImage = data.groupImage;
																						message.participantId = data.participantId;
																						message.createdBy = data.from;
																						message.addedBy = data.from;																		
																						message.cacheType = 'groupadd';																										
																						var receiverprefix = participant.userinfo._id.concat(":group");
																						// console.log('create group starts');
																						// console.log(receiverprefix);
																						// console.log(message);
																						// console.log('create group ends');
																							if(participant.userinfo._id === data.from)
																							{

																							}
																							else{
																								// if(!participant.userinfo.isOnline)														
																								// {
																								// 	var client = redis.createClient();																	
																								// 	client.rpush(participant.userinfo._id,JSON.stringify(message));
																								// }
																								// else{
																											// console.log('adding group sockt');
																									const uuidv1 = require('uuid/v1');
																	
																									message.uniqueid = uuidv1();
																									
																									var client = redis.createClient();																	
																									client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);			
																									socket.broadcast.emit(receiverprefix, message);
																									socket.emit(receiverprefix, message);				
																								// }
																							}
																				                	
																								
																					});							

																				}
																				else{
																					console.log('error');
																					var res = {};
																					res.error = "true";
																					res.message = "User not found";
																					cb(false)
																				}														
																		}
																});
											

																				
					    			            									
																				// sendGroupMessage(data.groupId,message,data.from);																																											

																});
			    			      //      						}										
				    			     //       					else{
				    			     //       						cb(false);
				    			     //       					}
			    			            				// }
			    			        				// });
			    			            }
			    			            else{
			    			            		var response = {};
			    			            		response.error = "true";
												response.success = false;
												response.message = 'A group with the group ID not found';
							            		// reply(response);
							            		callback(response);
			    			            }
									}
						});
	
				});
	
				socket.on('exitChannel',function(data,callback){
					var channelId = data.channelId;
					var participantId = data.from;
					var cb = callback;
					db.GroupParticipants.remove({
				            channelId: channelId,
				            participantId: participantId
				        }, function (err, result) {				

				            if (err) {
				                cb(false);
				            }				

				            else if (result.n === 0) {
				                cb(false);
				            }	
				            else{
				            	cb(true);
				            }			
				            db.ChannelParticipants.aggregate([
	  									{ "$match": { "channelId": data.channelId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
												// reply(doc);
														doc.forEach(function(participant) {		
															var message = {};
															
															message.contentType = 'GroupExit';
															message.content = 'GroupName edited';
															message.channelId = data.channelId;
															message.cacheType = 'groupadd';

															var timestamp = (new Date).getTime();
															message.time = timestamp;
															
															message.from = data.from;
																																												
															var receiverprefix = participant.userinfo._id.concat(":group");
															// console.log('exit group name starts');
															// console.log(receiverprefix);
															// console.log(message);
															// console.log(participant.userinfo.isOnline);
															// console.log('exit group name ends');
															if(participant.userinfo._id === data.from)
															{

															}
															else{
																// if(!participant.userinfo.isOnline)														
																// {
																	// console.log('stored in redis');
																// 	var client = redis.createClient();																	
																// 	client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }
																// else{
																	// console.log('adding group sockt');
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	

																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															}
														});
													}

												}	
										});
											
														
															            
				        });
				});
				socket.on('removeParticipantInChannel',function(data,callback){
					var channelId = data.channelId;
					var participantId = data.participantId;
					var cb = callback;
					db.ChannelParticipants.remove({
				            channelId: channelId,
				            participantId: participantId
				        }, function (err, result) {				

				            if (err) {
				                cb(false);
				            }				

				            if (result.n === 0) {
				                cb(false);
				            }	
				            else{
				            	cb(true);
				            }			
				            db.ChannelParticipants.aggregate([
	  									{ "$match": { "channelId": data.channelId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
												// reply(doc);
														doc.forEach(function(participant) {		
															var message = {};
															
															message.contentType = 'ChannelRemoveParticipant';
															message.content = 'Channel participant removed';
															message.channelId = data.channelId;
															

															var timestamp = (new Date).getTime();
															message.time = timestamp;
															
															message.from = data.from;
																																												
															message.cacheType = 'channeladd';
																																												
															var receiverprefix = participant.userinfo._id.concat(":channel");
															// console.log('exit group name starts');
															// console.log(receiverprefix);
															// console.log(message);
															// console.log(participant.userinfo.isOnline);
															// console.log('exit group name ends');
															// if(!participant.userinfo.isOnline)														
															// {
																// console.log('stored in redis');
																// var client = redis.createClient();																	
																// client.rpush(participant.userinfo._id,JSON.stringify(message));
															// }
															// else{
																// console.log('adding group sockt');
																const uuidv1 = require('uuid/v1');
																	
																message.uniqueid = uuidv1();
																	
																var client = redis.createClient();																	
																client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	

																socket.broadcast.emit(receiverprefix, message);
																socket.emit(receiverprefix, message);				
															// }
														});

													}
													else{
														console.log('error');
														var res = {};
														res.error = "true";
														res.message = "User not found";
														reply(res);
													}	
												}
										});	
				            
				        });
				});


				socket.on('removeAdminInChannel',function(data,callback){
					var channelId = data.channelId;
					var participantId = data.participantId;
					var cb = callback;
					var details = {};
					details.isAdmin = "user";
					db.ChannelParticipants.update({
		            	    channelId: channelId,
		            	    participantId: participantId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
		           		 		if(err)
		           		 		{
									cb(false);
		           		 		}
		           		 		else{
		           		 			if(result.n === 0){
										cb(false);
		           		 			}	
		           		 			else{
		           		 				if(cb)
		           		 				{
		           		 					cb(true);
		           		 				}
		           		 				db.ChannelParticipants.aggregate([
	  										{ "$match": { "channelId": data.channelId } },
											{ "$lookup": {
												"localField": "participantId",
												"from": "User",
												"foreignField": "_id",
												"as": "userinfo"
												}
											},
											{ "$unwind": "$userinfo" },
											{ "$project": {
												"text": 1,
												"date": 1,
												"userinfo": 1,
											} 
											}
											],(err, doc) => {																

												if (err) {
													return reply(Boom.wrap(err, 'Internal MongoDB error'));
												}
												else{
													if (doc) {																                
													// reply(doc);
															doc.forEach(function(participant) {		
																var message = {};
																
																message.contentType = 'ChannelRemoveAdmin';
																message.content = 'Channel admin';
																message.channelId = data.channelId;
																message.cacheType = 'channeladd';

																var timestamp = (new Date).getTime();
																message.time = timestamp;
																
																																													
																var receiverprefix = participant.userinfo._id.concat(":channel");
																// console.log('exit group name starts');
																// console.log(receiverprefix);
																// console.log(message);
																// console.log(participant.userinfo.isOnline);
																// console.log('exit group name ends');
																// if(!participant.userinfo.isOnline)														
																// {
																	// console.log('stored in redis');
																	// var client = redis.createClient();																	
																	// client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }
																// else{
																	// console.log('adding group sockt');
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															});	

														}
														else{
															console.log('error');
															var res = {};
															res.error = "true";
															res.message = "User not found";
															reply(res);
														}	
													}
											});	

		           		 			}	
		           		 		}
		           		 		
		           		 });
					 
				});
				socket.on('addAdminInChannel',function(data,callback){
					var channelId = data.channelId;
					var participantId = data.participantId;
					var cb = callback;
					var details = {};
					details.isAdmin = 1;
					details.madeAdminBy = data.from;
					console.log(data)
					db.ChannelParticipants.update({
		            	    channelId: channelId,
		            	    participantId: participantId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
		           		 		if(err)
		           		 		{		
		           		 			console.log(err);           		 			
									cb(false);
		           		 		}
		           		 		else{
		           		 			if(result.n === 0){
		           		 				console.log('No Rows found');
										cb(false);
		           		 			}	
		           		 			else{
		           		 				if(cb)
		           		 				{
		           		 					cb(true);
		           		 				}

		           		 				db.ChannelParticipants.aggregate([
	  										{ "$match": { "channelId": data.channelId } },
											{ "$lookup": {
												"localField": "participantId",
												"from": "User",
												"foreignField": "_id",
												"as": "userinfo"
												}
											},
											{ "$unwind": "$userinfo" },
											{ "$project": {
												"text": 1,
												"date": 1,
												"userinfo": 1,
											} 
											}
											],(err, doc) => {																

												if (err) {
													return reply(Boom.wrap(err, 'Internal MongoDB error'));
												}
												else{
													if (doc) {																                
													// reply(doc);
															doc.forEach(function(participant) {		
																var message = {};
																
																message.contentType = 'ChannelAddAdmin';
																message.content = 'Channel admin';
																message.channelId = data.channelId;
																message.cacheType = 'channeladd';

																var timestamp = (new Date).getTime();
																message.time = timestamp;
																
																message.from = data.from;
																																													
																var receiverprefix = participant.userinfo._id.concat(":channel");
																
																// if(!participant.userinfo.isOnline)														
																// {
																	// console.log('stored in redis');
																	// var client = redis.createClient();																	
																	// client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }	
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);																
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
															});	

														}
														else{
															console.log('error');
															var res = {};
															res.error = "true";
															res.message = "User not found";
															reply(res);
														}	
													}
											});	

		           		 			}	
		           		 		}
		           		 		
		           		 });
					 
				});

				socket.on('editChannel',function(data,callback){
					var details = {};
					var cb = callback;
					details.channelName = data.channelName;
					details.channelDescription = data.channelDescription;
					details.channelImage = data.channelImage;
					details.signAdmin = data.signAdmin;
					details.channelId = data.channelId;
					details.editedFields = data.editedFields;
					
					console.log(details);
					
					db.Channels.update({
		            	    channelId: data.channelId
        		   		 }, {
                			$set: details
		           		 }, function (err, result) {
									
    			    	        if (err) {
                				    console.log(err);
                				    details.error = true
                				    cb(false);
            	    			}
								else{
									if (result.n === 0) {  
										console.log('Not updated');          			    	    
										cb(false);
				            	    }
				            	    else{
				            	    	if(cb)
				            	    	{
				            	    		cb(true);	
				            	    	}
										db.ChannelParticipants.aggregate([
	  									{ "$match": { "channelId": data.channelId } },
										{ "$lookup": {
											"localField": "participantId",
											"from": "User",
											"foreignField": "_id",
											"as": "userinfo"
											}
										},
										{ "$unwind": "$userinfo" },
										{ "$project": {
											"text": 1,
											"date": 1,
											"userinfo": 1,
										} 
										}
										],(err, doc) => {															

											if (err) {
												return reply(Boom.wrap(err, 'Internal MongoDB error'));
											}
											else{
												if (doc) {																                
														doc.forEach(function(participant) {		
															var message = {};
															
															message.contentType = 'ChannelEdit';
															message.content = 'Channel edited';
															message.channelId = data.channelId;
															message.channelName = data.channelName;
															message.editedFields = data.editedFields;
															message.cacheType = 'channeladd';
															var timestamp = (new Date).getTime();
															message.time = timestamp;
															
															message.from = data.from;
																																												
															var receiverprefix = participant.userinfo._id.concat(":channel");
															if(participant.userinfo._id === data.from)
															{

															}
															else{
																// if(!participant.userinfo.isOnline)														
																// {
																// 	var client = redis.createClient();																	
																// 	client.rpush(participant.userinfo._id,JSON.stringify(message));
																// }
																// else{
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(participant.userinfo._id, message.uniqueid, JSON.stringify(message),redis.print);	
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
															}
														});

													}														
												}
										});									
										
				            	    }
                				    
								}
            			});		

				});

				socket.on('createChannel',function(data,callback){
					var timestamp = (new Date).getTime();
					var channelDetails = {};					

					var cb = callback;
			        channelDetails.name = data.channelName;
			        channelDetails.image = data.channelImage;
			        channelDetails.channelId = data.channelId;
			        channelDetails.channelType = data.channelType;
			        channelDetails.channelDescription = data.channelDescription;
			        channelDetails.createdBy = data.from;
			        channelDetails.signAdmin = false;
					channelDetails.createdAt = timestamp; 

					db.Channels.save(channelDetails, (err, result) => {
			                					if (err) {
			                						err.success = false;
			                    					return reply(err);
			                    					cb(false);
			                					}
			                					cb(true);
			                				var participants = JSON.parse(data.participants);
			                				// console.log(data);
			                				var count = 0;
			    							participants.forEach(function(participant) {
				    							var pid = participant.participantId;
				    							var isAdmin = participant.participantType;
												
				     							var participantDetails = {};
												participantDetails.channelId = data.channelId;
					    						participantDetails.joinedAt = timestamp;
					    						participantDetails.addedBy = data.from;
					     			            participantDetails.participantId = pid;
					     			            participantDetails.isAdmin = isAdmin;
					    			            					
												db.ChannelParticipants.save(participantDetails,(err,result) =>{
												if (err) {
					                				err.success = false;
					               					return reply(err);
					                			}   
					                			count++;					                			
												
												var message = {};
															
												var timestamp = (new Date).getTime();							

												message.contentType = 'ChannelAdd';
												message.content = 'You joined this channel';
												message.time = timestamp;
												message.channelId = data.channelId;
												message.channelName = data.channelName;
												message.channelImage = data.channelImage;
												message.channelType = data.channelType
												message.channelDescription = data.channelDescription;
												message.createdBy = data.from;																		
												message.cacheType = 'channeladd';				
																				
												var receiverprefix = pid.concat(":channel");
																				
												db.User.findOne({
					        		        		_id: pid
												}, (err, doc) => {															

													if (err) {
														return reply(Boom.wrap(err, 'Internal MongoDB error'));
													}
													else
													{										
														if (doc) 
															{	
																// if(!doc.isOnline)														
																// {
																	// var client = redis.createClient();																	
																	// client.rpush(pid,JSON.stringify(message));
																	const uuidv1 = require('uuid/v1');
																	
																	message.uniqueid = uuidv1();
																	
																	var client = redis.createClient();																	
																	client.hset(pid, message.uniqueid, JSON.stringify(message),redis.print);	
																// }
																// else{
																	socket.broadcast.emit(receiverprefix, message);
																	socket.emit(receiverprefix, message);				
																// }
																							
															}			
													}
												});		
					    			        	
					    			        	if(count == participants.length )
							    			    {							    			            						
													sendChannelMessage(data.channelId,message,data.from);																																											

											    }					

											});
							
					    			            		
			    						});
			            		});
				});
				socket.on('createGroup',function(data,callback){
						var timestamp = (new Date).getTime();
			            var groupDetails = {};			
			
						var cb = callback;

			            groupDetails.name = data.groupName;
			            groupDetails.image = data.groupImage;
			            groupDetails.groupId = data.groupId;
			            groupDetails.createdBy = data.from;
						groupDetails.createdAt = timestamp; 
						db.Groups.findOne({
							groupId: data.groupId
			    			}, (err, doc) => {
			    				if (err) {
			    			            	var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
			    			                return reply(Boom.wrap(err, 'Internal MongoDB error'));
			    			            }
										else{
			    			            if (!doc) {
			    			            	db.Groups.save(groupDetails, (err, result) => {
			                					if (err) {
			                						err.success = false;
			                    					return reply(err);
			                    					cb(false);
			                					}
			                					cb(true);
			                				var participants = JSON.parse(data.participants);
			                				// console.log(data);
			                				var count = 0;
			    							participants.forEach(function(participant) {
				    							var pid = participant.participantId;
				    							var isAdmin = participant.isAdmin;
												
				     							db.GroupParticipants.findOne({
					    			            		participantId: pid,
						    			            	groupId:data.groupId
					    			        		}, (err, doc) => {
					    			            			if (err) {
					    			            				var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
					    			                			return reply(Boom.wrap(err, 'Internal MongoDB error'));
					    			            			}
															else{	
					    			            				if (!doc) {
					    			            					var participantDetails = {};
																	participantDetails.groupId = data.groupId;
					    											participantDetails.joinedAt = timestamp;
					    											participantDetails.addedBy = data.from;
					     			            					participantDetails.participantId = pid;
					     			            					participantDetails.isAdmin = isAdmin;
					    			            					
																	db.GroupParticipants.save(participantDetails,(err,result) =>{
																		if (err) {
					                										err.success = false;
					               									     	return reply(err);
					                									}   
					                									count++;
					                									var response = {};
							    			            						response.error = "false";
																				response.success = true;			
							
											

																				var message = {};
															
																				var timestamp = (new Date).getTime();							

																				message.contentType = 'GroupAdd';
																				message.content = 'You are added to this group';
																				message.time = timestamp;
																				message.groupId = data.groupId;
																				message.groupName = data.groupName;
																				message.groupImage = data.groupImage;
																				message.createdBy = data.from;																		
																				message.cacheType = 'groupadd';				
																				
																				var receiverprefix = pid.concat(":group");
																				
																				db.User.findOne({
					        		        										_id: pid
																	            }, (err, doc) => {															

															        		        if (err) {
																	                    return reply(Boom.wrap(err, 'Internal MongoDB error'));
															                		}
																					else
																					{										
																	                	if (doc) 
																		                {	
																		      //           	if(!doc.isOnline)														
																								// {
																								// 	var client = redis.createClient();																	
																								// 	client.rpush(pid,JSON.stringify(message));
																								// }
																								// else{
																									const uuidv1 = require('uuid/v1');
																	
																									message.uniqueid = uuidv1();
																	
																									var client = redis.createClient();																	
																									client.hset(pid, message.uniqueid, JSON.stringify(message),redis.print);	
																									socket.broadcast.emit(receiverprefix, message);
																									socket.emit(receiverprefix, message);				
																								// }
																							
																        		        }			
															        		        }
																	            });		
					    			            							if(count == participants.length )
							    			            					{							    			            						
																				sendGroupMessage(data.groupId,message,data.from);																																											

											            				   	}					

																	});
					    			           					}							
					    			            					
					    			            				}
				    			        				});
			    								});
			            					});
			    			            }
			    			            else{
			    			            		var response = {};
			    			            		response.error = "true";
												response.success = false;
												response.message = 'A group with the same group ID already exists';												
			    			            }
									}
						});
				});


				socket.on('getOnline',function(data){
				
					var fromSender = data.from;
					var receiver = data.id;
					// console.log(data);
					db.User.findOne({
        		        _id: receiver
		            }, (err, doc) => {

        		        if (err) {
		                    return reply(Boom.wrap(err, 'Internal MongoDB error'));
                		}
						else{
		                if (doc) {

		                	var lastSeenPrivacy = doc.lastSeenPrivacy
		                	if(lastSeenPrivacy == 'e'){
		                		var status = doc.isOnline;
		                		var lastSeen = doc.lastSeen;
			                	
			                	var sendStatus = {};
			                	sendStatus.status = status;
			                	sendStatus.lastSeen = lastSeen;
			                	// console.log(sendStatus);
			                	if(fromSender)
			                	{
			                	var receiverprefix =fromSender.concat(":onlineStatus");
								socket.emit(receiverprefix, sendStatus);	
								}			                	
		                	}
		                	else if(lastSeenPrivacy == 'c'){
		                		db.UserFriends.findOne({
		                			from : receiver,
		                			to: fromSender
		                		},(err,docu) => {
		                			// console.log(docu);
		                			if(docu){
		                				var status = doc.isOnline;
		                				var lastSeen = doc.lastSeen;
					                	
					                	var sendStatus = {};
					                	sendStatus.status = status;
					                	sendStatus.lastSeen = lastSeen;
					                	// console.log(sendStatus);
					                	var receiverprefix =fromSender.concat(":onlineStatus");
										socket.emit(receiverprefix, sendStatus);
		                			}
		                			else{
		                				var status = 0;
		                				var lastSeen = "";
					                	
					                	var sendStatus = {};
					                	sendStatus.status = status;
					                	sendStatus.lastSeen = lastSeen;
					                	// console.log(sendStatus);
					                	var receiverprefix =fromSender.concat(":onlineStatus");
										socket.emit(receiverprefix, sendStatus);
		                			}

		                		});
		                		
		                	}
		                	else{
		                		var status = 0;
		                		var lastSeen = "";
			                	
			                	var sendStatus = {};
			                	sendStatus.status = status;
			                	sendStatus.lastSeen = lastSeen;
			                	// console.log(sendStatus);
			                	var receiverprefix =fromSender.concat(":onlineStatus");
								socket.emit(receiverprefix, sendStatus);
		                	}
		                	
        		          }

        		        }
		            });
				});
				
				
				// socket.on('groupMessageAck',function(data){
				// 	var ack = {};
				// 	ack.messageid = data.messageId;
				// 	ack.to = data.to;
				// 	var receiver =  data.from;
				// 	var sender =  data.to;
				// 	ack.from = data.from;
				// 	ack.time = data.time;
				// 	ack.cacheType = 'ack';
				// 	ack.status = "delivered";

				// 	console.log('ELEMENTS');
				// 	var client = redis.createClient();
				// 	client.hdel(data.to,data.messageId,redis.print);
				// });
				
				socket.on('ack',function(data,callback){

					var messageid = data.uniqueid;
					var userid = data.userid;
					var client = redis.createClient();
					console.log('ACK');
					console.log(data);
					client.hdel(userid,messageid,redis.print);					 
				});

				socket.on('sendDelivered',function(data){
					var ack = {};
					ack.messageid = data.messageId;
					ack.to = data.to;
					var receiver =  data.from;
					var sender =  data.to;
					ack.from = data.from;
					ack.time = data.time;
					ack.cacheType = 'ack';
					ack.status = "delivered";

					
					console.log('Chat Room Type');
					console.log(data.chatRoomType);
					console.log(data);
					var client = redis.createClient();
					client.hdel(data.userid,data.messageId,redis.print);					 
					

					

					var roomId = "";
					if(data.chatRoomType == 0)
					{
						if(sender<receiver)
						{
							roomId = sender.concat(receiver);
						}
						else{
							roomId = receiver.concat(sender);
						}	
						ack.roomId = roomId;

						var details = {};
						details.deliveredStatus = 1;
						db.ChatMessages.update({
		            		    _id: data.messageId
        		   		 	}, {
	                			$set: details
			           		 }, function(err,data){

		           		 	});


						db.User.findOne({
			        		        _id: data.from
					            }, (err, doc) => {			

			        		        if (err) {
					                    return reply(Boom.wrap(err, 'Internal MongoDB error'));
			                		}
									else
									{										
					                	if (doc) 
						                {	
						      //           	if(!doc.isOnline)
						      //           	{
												// var client = redis.createClient();																	
												// client.rpush(receiver,JSON.stringify(ack));	
												// client.hset(receiver, data.messageId, JSON.stringify(ack),redis.print);	
												// console.log('ack store in redis = '.concat(receiverprefix));													
											// }
											// else{
												const uuidv1 = require('uuid/v1');
																	
												ack.uniqueid = uuidv1();
																	
												var client = redis.createClient();																	
												client.hset(receiver, ack.uniqueid, JSON.stringify(ack),redis.print);	
												var receiverprefix =receiver.concat(":ack");
												socket.broadcast.emit(receiverprefix, ack);
												// console.log('delivered receiver = '.concat(receiverprefix));
											// }
											if(doc.isWebOnline)
						                	{
												var receiverprefix =receiver.concat(":ack");
												socket.broadcast.emit(receiverprefix, ack);
												// console.log('delivered receiver = '.concat(receiverprefix));
											}											

											
				        		        }			
			        		        }
					            });
					}
					else{
						roomId = receiver;
					}

					

					
				});
				

				socket.on('sendSeen',function(data){
					var ack = {};
					console.log(data);
					ack.messageid = data.messageId;
					ack.to = data.to;
					var receiver =  data.from;	
					var sender =  data.to;	
					ack.from = data.from;
					ack.time= data.time;
					ack.cacheType = 'ack';					
					ack.status = "read";


					var roomId = "";
					if(data.chatRoomType == 0)
					{
						if(sender<receiver)
						{
							roomId = sender.concat(receiver);
						}
						else{
							roomId = receiver.concat(sender);
						}	
					}
					else{
						roomId = receiver;
					}

					ack.roomId = roomId;

					var details = {};
					details.seenStatus = 1;
					db.ChatMessages.update({
		            	    _id: data.messageId
        		   		 }, {
                			$set: details
		           		 }, function(err,data){

		           		 }
		           		 );

					db.User.findOne({
			        		        _id: data.from
					            }, (err, doc) => {			

			        		        if (err) {
					                    return reply(Boom.wrap(err, 'Internal MongoDB error'));
			                		}
									else
									{										
					                	if (doc) 
						                {	
						                	// if(!doc.isOnline)
						                	// {
												// var client = redis.createClient();																	
												// client.rpush(receiver,JSON.stringify(ack));	
												// client.hset(receiver, data.messageId, JSON.stringify(ack),redis.print);																								
												// console.log('ack store in redis = '.concat(receiverprefix));
											// }
											// else{
											// 	var receiverprefix =receiver.concat(":ack");
											// 	socket.broadcast.emit(receiverprefix, ack);
											// 	console.log('seen receiver = '.concat(receiverprefix));
											// }
											// if(doc.isWebOnline)
						     //            	{
						     					const uuidv1 = require('uuid/v1');
																	
												ack.uniqueid = uuidv1();
																	
												var client = redis.createClient();																	
												client.hset(receiver, ack.uniqueid, JSON.stringify(ack),redis.print);	
												var receiverprefix =receiver.concat(":ack");
												socket.broadcast.emit(receiverprefix, ack);
												console.log('seen receiver = '.concat(receiverprefix));
											// }
				        		        }			
			        		        }
					            });										
				});
				
				socket.on('sendBroadcast',function(data){
					console.log(data);
					var participants = JSON.parse(data.participants);

					var count = 0;																	

			    	participants.forEach(function(participant) {
			    		var pid = participant.participantId;			    		

			    		db.User.findOne({
			    			_id: pid
			    		},(err,doc) => {
			    			if(err){
									var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
			    			        return reply(Boom.wrap(err, 'Internal MongoDB error'));
			    			}
			    			else{
			    				if(doc){

			    					var message = {};
									var ack = {};
									message.from = data.from;
									var sender = data.from;
									var receiver =  pid;
									// console.log('send Message starts');
									// console.log(data);
									// console.log('send Message ends');
									message.messageId = data.messageId;
									message.to = receiver;
									// message.name = data.name;
									// message.image = data.image;				

									message.content = data.content;
									message.time = data.time;												

													

									message.contentType = data.contentType;
									if(data.thumbnail)
									{
										message.thumbnail = data.thumbnail;
									}
									message.content = data.content;
									message.time = data.time;				

									if(data.contentType == 'location')
									{
										message.latitude = data.latitude;
										message.longitude = data.longitude;
									}				

									if(data.contentType == 'contact')
									{
										message.contactName = data.contactName;
										message.contactNumber = data.contactNumber;
									}				

									if(data.contentType == 'document')
									{
										message.documentName = data.documentName;						
									}				

									message.chatRoomType = data.chatRoomType;

			    					console.log('single chattttttttttttttttttttttttttttt 2');
									message.chatRoomType = 0;
									ack.messageid = data.messageId;
									ack.to = pid;
									ack.status = "sent";
									ack.time = data.time;
									ack.content = data.content;			
									if(data.thumbnail)
									{
										message.thumbnail = data.thumbnail;
									}
									console.log(ack);
									var receiverprefix =receiver.concat(":receiveMessage");
									// message.roomId = roomId;
									var webprefix = sender.concat(":receiveMessage");
									socket.broadcast.emit(receiverprefix, message);	

									message.userid = pid;	

									// console.log('recreiver = '.concat(receiverprefix));
									var ackprefix =sender.concat(":ack");
									console.log('ack = '.ack);				

									socket.emit(ackprefix,ack);
									// ack.roomId = roomId;
									socket.broadcast.emit(ackprefix,ack);
									message.contentType = data.contentType;
									socket.broadcast.emit(webprefix,message);
			    					//chatRoomType,groupId,from,contentType,messageId,content,time,to,latitude,longitude,groupname,create_by,sent_by,contactName,contactNumber

			    					// if(!doc.isOnline)
						      //           	{
												
												var message = {};
												message.from = data.from;
										
												message.messageId = data.messageId;
												message.to = pid;
						
												message.contentType = data.contentType;
												message.content = data.content;
												message.time = data.time;
												message.chatRoomType = 0;
												message.cacheType = 'chat';
												if(data.thumbnail)
												{
													message.thumbnail = data.thumbnail;
												}
												var client = redis.createClient();
												// console.log('rpush2');
												// client.rpush(pid,JSON.stringify(message));			
												client.hset(pid, message.messageId, JSON.stringify(message),redis.print);

											// }

											var FCM = require('fcm-node');
												
												var message = {};
												message.from = data.from;
												var sender = data.from;
												var receiver =  pid;
										
												message.messageId = data.messageId;
												message.to = receiver;
						
												message.contentType = data.contentType;
												var content = '';
												if(data.contentType == 'image')
												{
													content = '📷 Image';	
												}
												else if(data.contentType == 'video')
												{
													content = '📹 Video';	
												}
												else if(data.contentType == 'contact')
												{
													content = '👤 Contact';	
												}
												else if(data.contentType == 'audio')
												{
													content = '🔈 Audio';	
												}
												else if(data.contentType == 'location')
												{
													content = '📌 Location';	
												}
												else if(data.contentType == 'document')
												{
													content = '📄 Document';	
												}
												else{
													content = data.content;		
												}
												message.content = content;
												message.time = data.time;
												message.chatRoomType = 0;
												if(data.thumbnail)
												{
													message.thumbnail = data.thumbnail;
												}
												// console.log('message')
												// console.log(message)
												message.cacheType = 'chat';
												

												var serverKey = PushServerKey;//'AAAAAuaSkcc:APA91bGWsMRs1WK2Z9SPoh0Mo4lHr2uZ0pF6qLSFUBLjyihzjV35S4koza93rayhcZfq0vlp8ZP9ItgJf2Jy8dh-1C9brkQzw2U2PQXQDngmGsi-2i913UhJZ7pMjuI19KGfzlJyp7ma';
												var fcm = new FCM(serverKey);			

												var gmessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								    				to: doc.pushNotificationId,								    				
								    				data: {  //you can send only notification or only data(or include both)
												        title: data.id,
												        message: content,
												        fromId: data.from,
												        pushType: 'message',
												        payload: message
				    								},priority : 'high',
				    								content_available : true
												};	
												// console.log('before push');
												// console.log(gmessage);
												fcm.send(gmessage, function(err, response){
												    if (err) {

										    		    console.log("Something has gone wrong!");
										    		    console.log(err);
												    } else {
							    				    	console.log("Successfully sent with response: ", response);
						    						}
												});

			    				}			    							    				
			    			}
			    		});

			    	});

				});

				
				socket.on('sendMessageInChannel',function(data){
					var message = {};
					var ack = {};
					message.from = data.from;
					var sender = data.from;
					var receiver =  data.to;
					var notify = data.notify;
					message.shouldSign = data.shouldSign;
					console.log('Send Channel Message starts');
					console.log(data);
					console.log('Send Channel Message ends');
					message.messageId = data.messageId;
					message.to = receiver;

					message.content = data.content;
					message.time = data.time;
					

					var chatroom1 ={};
					chatroom1.from = sender;
					chatroom1.to = receiver;
					var roomId = "";					
					roomId = receiver;					
					
					chatroom1.roomId = roomId; 
					chatroom1.lastMessage = data.content;
					chatroom1.lastMessageTime = data.time;
					chatroom1.chatRoomType = data.chatRoomType;


 					db.Chatrooms.update({from: sender,to:receiver}, {"$setOnInsert": chatroom1}, {upsert: true},function(err,doc)
						{
							if(err)
							{
								console.log(err);
							}
							else{
								console.log(doc);
							}
						});
					db.Chatrooms.update( {from:sender,to:receiver}, { $inc : { "totalMessages" : 1 } });



					db.ChatMessages.find({roomId: roomId}).limit(1).sort({messageCount:-1},function(err, doc) { 
							if(err)
							{
							   err.success = false;
			                   console.log('Message Not Inserted');
							}
							console.log('CHAT MSG FIND');
							if(!doc) 
							{
								var message = {};
								message.roomId = roomId;
								message.content = data.content;
								message.contentType = data.contentType;
								message.isStarred = 0;
								message.from = sender;
								message.to = receiver;
								message.deliveredStatus = 0;
								message._id = data.messageId;
								message.seenStatus = 0;
								message.sentTime = data.time
								message.messageCount = 1;
											
								message.contentType = data.contentType;

								if(data.thumbnail)
								{
									message.thumbnail = data.thumbnail;
								}

								if(data.contentType == 'location')
								{
									message.latitude = data.latitude;
									message.longitude = data.longitude;
								}

								if(data.contentType == 'contact')
								{
									message.contactName = data.contactName;
									message.contactNumber = data.contactNumber;
								}

								if(data.contentType == 'document')
								{
									message.documentName = data.documentName;						
								}


								db.ChatMessages.save(message , (err, result) => {
			                		if (err) {
						               	err.success = false;
						                   console.log('Message Not Inserted');
						               }
						           	console.log('Message Inserted');
						          });
							}
							else if(doc.length === 0)
							{
								var message = {};
								message.roomId = roomId;
								message.content = data.content;
								message.contentType = data.contentType;
								message.isStarred = 0;
								message.from = sender;
								message.to = receiver;
								message.deliveredStatus = 0;
								message._id = data.messageId;
								message.seenStatus = 0;
								message.sentTime = data.time
								message.messageCount = 1;
											
								message.contentType = data.contentType;

								if(data.thumbnail)
								{
									message.thumbnail = data.thumbnail;
								}

								if(data.contentType == 'location')
								{
									message.latitude = data.latitude;
									message.longitude = data.longitude;
								}

								if(data.contentType == 'contact')
								{
									message.contactName = data.contactName;
									message.contactNumber = data.contactNumber;
								}

								if(data.contentType == 'document')
								{
									message.documentName = data.documentName;						
								}


								db.ChatMessages.save(message , (err, result) => {
			                		if (err) {
						               	err.success = false;
						                   console.log('Message Not Inserted');
						               }
						           	console.log('Message Inserted');
						          });
							}
							else{
								var message = {};
								message.roomId = roomId;
								message.content = data.content;
								message.contentType = data.contentType;
								message.isStarred = 0;
								message.from = sender;
								message.to = receiver;
								message.deliveredStatus = 0;
								message._id = data.messageId;
								message.seenStatus = 0;
								message.sentTime = data.time
								console.log('Message Count');
								console.log(doc);
								message.messageCount = parseInt(doc[0].messageCount)+1;
								
								message.contentType = data.contentType;

								if(data.thumbnail)
								{
									message.thumbnail = data.thumbnail;
								}

								if(data.contentType == 'location')
								{
									message.latitude = data.latitude;
									message.longitude = data.longitude;
								}

								if(data.contentType == 'contact')
								{
									message.contactName = data.contactName;
									message.contactNumber = data.contactNumber;
								}

								if(data.contentType == 'document')
								{
									message.documentName = data.documentName;						
								}
			

								db.ChatMessages.save(message , (err, result) => {
			                		if (err) {
						               	err.success = false;
						                   console.log('Message Not Inserted');
						               }
						           	console.log('Message Inserted');
						          });
							}
					 });



					

					message.contentType = data.contentType;
					if(data.thumbnail)
					{
						message.thumbnail = data.thumbnail;
					}
					message.content = data.content;
					message.time = data.time;

					if(data.contentType == 'location')
					{
						message.latitude = data.latitude;
						message.longitude = data.longitude;
					}

					if(data.contentType == 'contact')
					{
						message.contactName = data.contactName;
						message.contactNumber = data.contactNumber;
					}

					if(data.contentType == 'document')
					{
						message.documentName = data.documentName;						
					}

					message.chatRoomType = data.chatRoomType;							


					message.channelId = data.to;
					message.chatRoomType = 3;
					message.roomId = roomId;
					ack.messageid = data.messageId;
					ack.to = data.to;
					ack.status = "sent";
					ack.time = data.time;
					ack.content = data.content;	
					ack.chatRoomType =3;
					console.log(ack);								
					var ackprefix = sender.concat(":ack");
					console.log('ack = '.ack);			

					socket.emit(ackprefix,ack);

					ack.roomId = roomId;
					socket.broadcast.emit(ackprefix,ack);

					db.ChannelParticipants.find({
						channelId: data.to
					}, (err, doc) => {
					    if (err) {
					    	var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
					    	return reply(Boom.wrap(err, 'Internal MongoDB error'));
					    }
						else{
					    	if (doc.length === 0) {									
								console.log('doc not found');
							}
							else{
								doc.forEach(function(participant) {					
									if(participant.participantId != data.from)
									{
										db.User.findOne({
							        		_id: participant.participantId
									    }, (err, user) => {							

							        		if (err) {
									        	return reply(Boom.wrap(err, 'Internal MongoDB error'));
							                }
											else{
												if(user)
												{					
													if(user.isOnline)
													{
														var gmessage = {};
														gmessage.from = data.from;
														var sender = data.from;
														var receiver =  user._id;
																		
														gmessage.messageId = data.messageId;
														gmessage.to = receiver;
														
														gmessage.contentType = data.contentType;
														gmessage.content = data.content;																				
																				
														gmessage.time = data.time;
																				
														if(data.thumbnail)
														{
															gmessage.thumbnail = data.thumbnail;
														}		

														console.log('message')
														console.log(gmessage)
														gmessage.cacheType = 'chat';
														gmessage.chatRoomType = 3;
														gmessage.channelId = data.to;		

														var client = redis.createClient();
														
														// client.hset(receiver, gmessage.messageId, JSON.stringify(gmessage),redis.print);
								                		
								                		const uuidv1 = require('uuid/v1');
																	
														gmessage.uniqueid = uuidv1();
																	
														var client = redis.createClient();																	
														client.hset(receiver, gmessage.uniqueid, JSON.stringify(gmessage),redis.print);	
																														
														var receiver =  user._id;
														var receiverprefix = receiver.concat(":receiveMessage");
																			
														socket.broadcast.emit(receiverprefix, message);
														console.log('recreiver = '.concat(receiverprefix));
													}			                								
														
													
													
														if(notify == 1)
														{
															var FCM = require('fcm-node');
											
															var gmessage = {};
															gmessage.from = data.from;
															var sender = data.from;
															var receiver =  user._id;
																				
															gmessage.messageId = data.messageId;
															gmessage.to = receiver;
															var content = '';
															gmessage.contentType = data.contentType;
															if(data.contentType == 'image')
															{
																content = '📷 Image';	
															}
															else if(data.contentType == 'video')
															{
																content = '📹 Video';	
															}
															else if(data.contentType == 'contact')
															{
																content = '👤 Contact';	
															}
															else if(data.contentType == 'audio')
															{
																content = '🔈 Audio';	
															}
															else if(data.contentType == 'location')
															{
																content = '📌 Location';	
															}
															else if(data.contentType == 'document')
															{
																content = '📄 Document';	
															}
															else if(data.contentType == 'sticker')
															{
																content = '🔖 sticker';	
															}
															else
															{
																content = data.content;
															}
															gmessage.time = data.time;
															if(data.thumbnail)
															{
																gmessage.thumbnail = data.thumbnail;
															}
															
															console.log('message')
															console.log(gmessage)
															gmessage.content = content;
															gmessage.cacheType = 'chat';
															gmessage.chatRoomType = 3;
																						// gmessage.channelName = data.channelName;
															gmessage.channelId = data.to;				

										                	var FCM = require('fcm-node');									

															var serverKey = PushServerKey;//'AIzaSyBt5ARj3AQ3kciykmMx7dIM9MKEyXCQndM';
															var fcm = new FCM(serverKey);									

															var timestmp = (new Date).getTime();									

															var pushType = 'ChannelAdd';
																		
															var pmessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
																to: user.pushNotificationId,
																data: {  //you can send only notification or only data(or include both)
																	title: data.id,
																	message: content,
																	fromId: data.from,
																	pushType: 'message',
																	payload: gmessage
														    	},priority : 'high',
														    	content_available : true
															};					

															console.log(pmessage);
															if(data.from ==  participant.participantId){
																	console.log('NOT SENDING');
															}
															else{
																fcm.send(pmessage, function(err, response){
																	if (err) {
																		console.log(err);
																	} else {
																		console.log("Successfully sent with response: ", response);
															    	}
																});
															}		

														}
												}
												else{
													console.log('error');
													var res = {};
													res.error = "true";
													res.message = "User not found";
												}							

							        		}
									    });																	
									}
								
							});
														// var webprefix = sender.concat(":receiveMessage");
														// message.roomId = roomId;
														// socket.broadcast.emit(webprefix,message);
						}
					}
				});


				});
				socket.on('sendMessage',function(data){

					console.log('send Message starts');
					console.log(data);
					console.log('send Message ends');

					var message = {};
					var ack = {};
					message.from = data.from;
					var sender = data.from;
					var receiver =  data.to;
					// data.content = "📄😋 ";
					
					message.showPreview = data.showPreview;
					if(message.showPreview){
						message.metaTitle = data.metaTitle;
						message.metaDescription = data.metaDescription;
						message.metaLogo = data.metaLogo;	
					}
					else{
						message.showPreview = "0";
					}
					


					message.messageId = data.messageId;
					message.to = receiver;


					message.content = data.content;
					message.time = data.time;
					

					var chatroom1 ={};
					chatroom1.from = sender;
					chatroom1.to = receiver;
					var roomId = "";
					if(data.chatRoomType == 0)
					{
						if(sender<receiver)
						{
							roomId = sender.concat(receiver);
						}
						else{
							roomId = receiver.concat(sender);
						}	
					}
					else{
						roomId = receiver;
					}
					
					chatroom1.roomId = roomId; 
					chatroom1.lastMessage = data.content;
					chatroom1.lastMessageTime = data.time;
					chatroom1.chatRoomType = data.chatRoomType;

					
					var chatroom2 = {};
					chatroom2.from = receiver;
					chatroom2.to = sender;
					chatroom2.roomId = roomId;
					chatroom2.lastMessage = data.content;
					chatroom2.lastMessageTime = data.time;
					chatroom2.chatRoomType = data.chatRoomType;
 					db.Chatrooms.update({from: sender,to:receiver}, {"$setOnInsert": chatroom1}, {upsert: true},function(err,doc)
						{
							if(err)
							{
								console.log(err);
							}
							else{
								// console.log(doc);
							}
						});
					db.Chatrooms.update({from: receiver,to:sender}, {"$setOnInsert": chatroom2}, {upsert: true});
					db.Chatrooms.update( {from:sender,to:receiver}, { $inc : { "totalMessages" : 1 } });



					db.ChatMessages.find({roomId: roomId}).limit(1).sort({messageCount:-1},function(err, doc) { 
							if(err)
							{
							   err.success = false;
			                   console.log('Message Not Inserted');
							}
							// console.log('CHAT MSG FIND');
							if(!doc) 
							{
								var message = {};
								message.roomId = roomId;
								message.content = data.content;
								message.contentType = data.contentType;
								message.isStarred = 0;
								message.from = sender;
								message.to = receiver;
								message.deliveredStatus = 0;
								message._id = data.messageId;
								message.seenStatus = 0;
								message.sentTime = data.time
								message.messageCount = 1;
											

								db.ChatMessages.save(message , (err, result) => {
			                		if (err) {
						               	err.success = false;
						                   console.log('Message Not Inserted');
						               }
						           	console.log('Message Inserted');
						          });
							}
							else if(doc.length === 0)
							{
								var message = {};
								message.roomId = roomId;
								message.content = data.content;
								message.contentType = data.contentType;
								message.isStarred = 0;
								message.from = sender;
								message.to = receiver;
								message.deliveredStatus = 0;
								message._id = data.messageId;
								message.seenStatus = 0;
								message.sentTime = data.time
								message.messageCount = 1;
											

								db.ChatMessages.save(message , (err, result) => {
			                		if (err) {
						               	err.success = false;
						                   console.log('Message Not Inserted');
						               }
						           	// console.log('Message Inserted');
						          });
							}
							else{
								var message = {};
								message.roomId = roomId;
								message.content = data.content;
								message.contentType = data.contentType;
								message.isStarred = 0;
								message.from = sender;
								message.to = receiver;
								message.deliveredStatus = 0;
								message._id = data.messageId;
								message.seenStatus = 0;
								message.sentTime = data.time
								// console.log('Message Count');
								// console.log(doc);
								message.messageCount = parseInt(doc[0].messageCount)+1;
											

								db.ChatMessages.save(message , (err, result) => {
			                		if (err) {
						               	err.success = false;
						                   console.log('Message Not Inserted');
						               }
						           	// console.log('Message Inserted');
						          });
							}
					 });



					

					message.contentType = data.contentType;
					if(data.thumbnail)
					{
						message.thumbnail = data.thumbnail;
					}
					message.content = data.content;
					message.time = data.time;

					if(data.contentType == 'location')
					{
						message.latitude = data.latitude;
						message.longitude = data.longitude;
					}

					if(data.contentType == 'contact')
					{
						message.contactName = data.contactName;
						message.contactNumber = data.contactNumber;
					}

					if(data.contentType == 'document')
					{
						message.documentName = data.documentName;						
					}

							message.chatRoomType = data.chatRoomType;							
						if(data.chatRoomType == 1)
						{
							console.log('group chattttttttttttttttttttttttttttt');
								message.groupId = data.to;
								message.chatRoomType = 1;
								message.roomId = roomId;
								ack.messageid = data.messageId;
								ack.to = data.to;
								ack.status = "sent";
								ack.time = data.time;
								ack.content = data.content;	
								ack.chatRoomType =1;
								// console.log(ack);								
								var ackprefix = sender.concat(":ack");
								// console.log('ack = '.ack);			

								socket.emit(ackprefix,ack);

								ack.roomId = roomId;
								socket.broadcast.emit(ackprefix,ack);

								db.GroupParticipants.find({
									groupId: data.to
					    			}, (err, doc) => {
					    				if (err) {
					    			            	var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
					    			                return reply(Boom.wrap(err, 'Internal MongoDB error'));
					    			            }
												else{
					    			            	if (doc.length === 0) {									
														console.log('doc not found');
													}
													else{
														doc.forEach(function(participant) {					
															if(participant.participantId != data.from)
															{
																db.User.findOne({
					        		    						    _id: participant.participantId
									            				}, (err, user) => {							

							        		    				    if (err) {
									            				        return reply(Boom.wrap(err, 'Internal MongoDB error'));
							                						}
																	else{
																		if(user)
																		{			
																			// if(!user.isOnline)					             
																			// {
																				var gmessage = {};
																				gmessage.from = data.from;
																				var sender = data.from;
																				var receiver =  user._id;
																		
																				gmessage.messageId = data.messageId;
																				gmessage.to = receiver;
														
																				gmessage.contentType = data.contentType;
																				gmessage.content = data.content;																				
																				
																				gmessage.time = data.time;
																				
																				if(data.thumbnail)
																				{
																					gmessage.thumbnail = data.thumbnail;
																				}		

																				// console.log('message')
																				// console.log(gmessage)
																				gmessage.cacheType = 'chat';
																				gmessage.chatRoomType = 1;
																				gmessage.groupId = data.to;		

																				var client = redis.createClient();
																				// console.log('rpush1');
																				// client.rpush(receiver,JSON.stringify(gmessage));
																			// }	
																			console.log('PUSHED');
																			console.log(gmessage);
																			client.hset(receiver, gmessage.messageId, JSON.stringify(gmessage),redis.print);
							                								
																																					
																			var receiver =  user._id;
																			var receiverprefix = receiver.concat(":receiveMessage");																	
																			socket.broadcast.emit(receiverprefix, message);
																			console.log('recreiver = '.concat(receiverprefix));

																			var FCM = require('fcm-node');
										
																			var gmessage = {};
																			gmessage.from = data.from;
																			var sender = data.from;
																			var receiver =  user._id;
																	
																			gmessage.messageId = data.messageId;
																			gmessage.to = receiver;
																			var content = '';
																			gmessage.contentType = data.contentType;
																			if(data.contentType == 'image')
																			{
																				content = '📷 Image';	
																			}
																			else if(data.contentType == 'video')
																			{
																				content = '📹 Video';	
																			}
																			else if(data.contentType == 'contact')
																			{
																				content = '👤 Contact';	
																			}
																			else if(data.contentType == 'audio')
																			{
																				content = '🔈 Audio';	
																			}
																			else if(data.contentType == 'location')
																			{
																				content = '📌 Location';	
																			}
																			else if(data.contentType == 'document')
																			{
																				content = '📄 Document';	
																			}
																			else if(data.contentType == 'sticker')
																			{
																				content = '🔖 sticker';	
																			}
																			else
																			{
																				content = data.content;
																			}
																			gmessage.time = data.time;
																			if(data.thumbnail)
																			{
																			gmessage.thumbnail = data.thumbnail;
																			}
																			// console.log('message')
																			// console.log(gmessage)
																			gmessage.content = content;
																			gmessage.cacheType = 'chat';
																			gmessage.chatRoomType = 1;
																			gmessage.groupName = 
																			gmessage.groupId = data.to;	

							                								var FCM = require('fcm-node');						

																			var serverKey = PushServerKey;//'AIzaSyBt5ARj3AQ3kciykmMx7dIM9MKEyXCQndM';
																			var fcm = new FCM(serverKey);						

																			var timestmp = (new Date).getTime();						

																			var pushType = 'GroupAdd';
															
																			var pmessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
														    					to: user.pushNotificationId,
															    				data: {  //you can send only notification or only data(or include both)
																			        title: data.id,
																			        message: content,
																			        fromId: data.from,
																			        pushType: 'message',
																			        payload: gmessage
											    								},priority : 'high',
											    								content_available : true
																			};		

																			// console.log(pmessage);
																			if(data.from ==  participant.participantId){
																					console.log('NOT SENDING');
																			}
																			else{
																				fcm.send(pmessage, function(err, response){
																			    	if (err) {
																						// console.log(err);
																				    } else {
																				        // console.log("Successfully sent with response: ", response);
													    							}
																				});
																			}
																	}
																	else{
																		// console.log('error');
																		var res = {};
																    	res.error = "true";
																    	res.message = "User not found";
																	}					

					        		        				}
							           					 });
														}
																															
													});
														// var webprefix = sender.concat(":receiveMessage");
														// message.roomId = roomId;
														// socket.broadcast.emit(webprefix,message);
													}
												}
									});
						}	
						else{
								console.log('single chattttttttttttttttttttttttttttt 2');
								message.chatRoomType = 0;
								ack.messageid = data.messageId;
								ack.to = data.to;
								ack.status = "sent";
								ack.time = data.time;
								ack.content = data.content;			
								if(data.thumbnail)
								{
									message.thumbnail = data.thumbnail;
								}
								// console.log(ack);
								var receiverprefix =receiver.concat(":receiveMessage");
								message.roomId = roomId;
								var webprefix = sender.concat(":receiveMessage");
								socket.broadcast.emit(webprefix,message);
								socket.broadcast.emit(receiverprefix, message);
								console.log('recreiver = '.concat(receiverprefix));
								var ackprefix =sender.concat(":ack");
								// console.log('ack = '.ack);			

								socket.emit(ackprefix,ack);
								ack.roomId = roomId;
								socket.broadcast.emit(ackprefix,ack);

								db.User.findOne({
			        		        _id: data.to
					            }, (err, doc) => {			

			        		        if (err) {
					                    return reply(Boom.wrap(err, 'Internal MongoDB error'));
			                		}
									else
									{										
					                	if (doc) 
						                {	
						                	// if(!doc.isOnline)
						                	// {
												
												var rmessage = {};
												rmessage.from = data.from;
										
												rmessage.messageId = data.messageId;
												rmessage.to = data.to;
						
												rmessage.contentType = data.contentType;
												rmessage.content = data.content;
												rmessage.time = data.time;
												rmessage.chatRoomType = 0;
												rmessage.cacheType = 'chat';
												if(data.thumbnail)
												{
													rmessage.thumbnail = data.thumbnail;
												}
												var client = redis.createClient();

												console.log('MESSAGEEEEEEEE');
												console.log(data.to);
												console.log(rmessage.messageId);
												var stringi = JSON.stringify(rmessage);
												console.log(stringi);
												client.hset(data.to, data.messageId, stringi,redis.print);
												// client.rpush(data.to,JSON.stringify(rmessage));			

											// }

											var FCM = require('fcm-node');
												
												var message = {};
												message.from = data.from;
												var sender = data.from;
												var receiver =  data.to;
										
												message.messageId = data.messageId;
												message.to = receiver;
						
												message.contentType = data.contentType;
												var content = '';
												if(data.contentType == 'image')
												{
													content = '📷 Image';	
												}
												else if(data.contentType == 'video')
												{
													content = '📹 Video';	
												}
												else if(data.contentType == 'contact')
												{
													content = '👤 Contact';	
												}
												else if(data.contentType == 'audio')
												{
													content = '🔈 Audio';	
												}
												else if(data.contentType == 'location')
												{
													content = '📌 Location';	
												}
												else if(data.contentType == 'document')
												{
													content = '📄 Document';	
												}
												else if(data.contentType == 'sticker')
												{
													content = '🔖 sticker';	
												}
												else{
													content = data.content;		
												}
												message.content = content;
												message.time = data.time;
												message.chatRoomType = 0;
												if(data.thumbnail)
												{
													message.thumbnail = data.thumbnail;
												}
												// console.log('message')
												// console.log(message)
												message.cacheType = 'chat';
												

												var serverKey = PushServerKey;//'AIzaSyBt5ARj3AQ3kciykmMx7dIM9MKEyXCQndM';
												var fcm = new FCM(serverKey);			

												var gmessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								    				to: doc.pushNotificationId,								    				
								    				data: {  //you can send only notification or only data(or include both)
												        title: data.id,
												        message: content,
												        fromId: data.from,
												        pushType: 'message',
												        payload: message
				    								},priority : 'high',
				    								content_available : true
												};	
												// console.log('before push');
												// console.log(gmessage);
												fcm.send(gmessage, function(err, response){
												    if (err) {

										    		    console.log("Something has gone wrong!");
										    		    console.log(err);
												    } else {
							    				    	// console.log("Successfully sent with response: ", response);
						    						}
												});
				        		        }			
			        		        }
					            });

							}	
					
					
					// }
				
				});
				
 });
  
	



var sendChannelMessage = function (channelId,message,createdby) {
			console.log('SEND MESSAGE');
			console.log(channelId);
			console.log(message);
			console.log(createdby);
			console.log('SEND MESSAGE END');
			db.ChannelParticipants.find({
				channelId: channelId
    			}, (err, doc) => {
    				if (err) {
    			            	var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
    			                return reply(Boom.wrap(err, 'Internal MongoDB error'));
    			            }
							else{
    			            if (doc.length === 0) {									
									console.log('doc not found');
								}
								else{
									doc.forEach(function(participant) {

    			            		var receiver = participant.participantId;
    			            		if(receiver === createdby)
    			            		{
		            						console.log('CONSOLE OUTPUT USER CREATED');
    			            		}
    			            		else
    			            		{
															db.User.findOne({
						        		    				    _id: receiver
								            				}, (err, user) => {						

						        		    				    if (err) {
								            				        return reply(Boom.wrap(err, 'Internal MongoDB error'));
						                						}
																else{
								            				    if (user) {
								                
						                								var FCM = require('fcm-node');						

																		var serverKey = PushServerKey;//'AIzaSyBt5ARj3AQ3kciykmMx7dIM9MKEyXCQndM';
																		var fcm = new FCM(serverKey);						

																		var timestmp = (new Date).getTime();						

																		var pushType = 'ChannelAdd';
														
																		var pmessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
																		    to: user.pushNotificationId,     																    
						    
																	    data: {  //you can send only notification or only data(or include both)
									    								    channelId: channelId,
									    								    channelName: message.channelName,
									    								    content: message.content,
									    								    contentType: message.contentType,
									    								    cacheType: message.cacheType,
									    								    time:message.time,
									    								    createdBy: message.createdBy,
																	        pushType : pushType
																	    },
																	    priority : 'high',
										    							content_available : true
																	};						

																	console.log(pmessage);
																	fcm.send(pmessage, function(err, response){
																    if (err) {
																    	var res = {};
																    	res.error = "true";
																        console.log("Something has gone wrong!");
																		console.log(err);
																    } else {
																		var res = {};
																    	res.error = "false";
																        console.log("Successfully sent with response: ", response);
									    							}
																});
							
													}
													else{
														console.log('error');
														var res = {};
												    	res.error = "true";
												    	res.message = "User not found";
													}						

						        		        }
								            });		
		            					}	
									});
								}
							}
						});
    
};


var sendGroupMessage = function (groupId,message,createdby) {
			console.log('SEND MESSAGE');
			console.log(groupId);
			console.log(message);
			console.log(createdby);
			console.log('SEND MESSAGE END');
			db.GroupParticipants.find({
				groupId: groupId
    			}, (err, doc) => {
    				if (err) {
    			            	var error = Boom.create(400, 'Bad request', { timestamp: Date.now() });
    			                return reply(Boom.wrap(err, 'Internal MongoDB error'));
    			            }
							else{
    			            if (doc.length === 0) {									
									console.log('doc not found');
								}
								else{
									var client = redis.createClient();
									doc.forEach(function(participant) {

    			            		var receiver = participant.participantId;
    			            		if(receiver === createdby)
    			            		{
		            						console.log('CONSOLE OUTPUT USER CREATED');
    			            		}
    			            		else
    			            		{
										db.User.findOne({
						        		       _id: receiver
								           }, (err, user) => {						

						        		       if (err) {
								                   return reply(Boom.wrap(err, 'Internal MongoDB error'));
						                	}
											else{
								                if (user) {
								                
						                			var FCM = require('fcm-node');						

													var serverKey = PushServerKey;//'AIzaSyBt5ARj3AQ3kciykmMx7dIM9MKEyXCQndM';
													var fcm = new FCM(serverKey);						

													var timestmp = (new Date).getTime();						

													var pushType = 'GroupAdd';
														
													var pmessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
													    to: user.pushNotificationId,     																    
						    
												    	data: {  //you can send only notification or only data(or include both)
										    			    groupId: groupId,
										    			    groupName: message.groupName,
										    			    content: message.content,
										    			    contentType: message.contentType,
										    			    cacheType: message.cacheType,
										    			    time:message.time,
										    			    createdBy: message.createdBy,
													        pushType : pushType
													    },
													    priority : 'high',
											   			content_available : true
													};						

													console.log(pmessage);
													fcm.send(pmessage, function(err, response){
													    if (err) {
													    	var res = {};
													    	res.error = "true";
													        console.log("Something has gone wrong!");
															console.log(err);
													    } else {
															var res = {};
													    	res.error = "false";
													        console.log("Successfully sent with response: ", response);
									    				}
													});
							
												}
												else{
														console.log('error');
														var res = {};
												    	res.error = "true";
												    	res.message = "User not found";
												}						

						        		    }
								        });		
		            				}	
								});
							}
						}
					});
   
};


   









    return next();
};



exports.register.attributes = {
    name: 'routes-user'
};
